package utils

import java.io.File
import org.apache.commons.io.filefilter.{ DirectoryFileFilter, NotFileFilter, TrueFileFilter }

import scala.collection.JavaConverters._

object FileUtils {
  /**
    * Get the list of files in a directory
    */
  def getListOfFiles(dir: String): List[File] = {
    val d = new File(dir)
    if (d.exists && d.isDirectory()) {
      d.listFiles().filter(_.isFile()).toList
    } else {
      List[File]()
    }
  }

  /**
    * Find files recursively
    * @param d base directory/file to search from
    * @param pattern the pattern to search for
    * @param wantDir Do we want to get directories (and files) in the result?
    * @return List of file/directory names matching all the conditions
    */
  def findFileOrDirRecursively(d: File, pattern: String, wantDir: Boolean): List[String] = {
    if (d.exists && d.isDirectory) {
      val files = d.listFiles
        .filter({f: File => if (wantDir) f.isDirectory else f.isFile})
        .filter({f: File => pattern.r.findFirstIn(f.getName).isDefined})  // match the pattern
        .map(_.getCanonicalPath)
        .toList
      val subdirs = d.listFiles.filter(_.isDirectory) // all directory
      val filesInSubDirs = subdirs
        .flatMap({dir: File => findFilesRecursively(dir, pattern)})
      files ++ filesInSubDirs
    } else if (d.exists && d.isFile && !wantDir) List(d.getCanonicalPath)
    else List()
  }

  def findFilesRecursively(d: File, pattern: String): List[String] =
    findFileOrDirRecursively(d, pattern, false)
  def findFilesRecursively(d: String, pattern: String): List[String] =
    findFilesRecursively(new File(d), pattern)
  def findSubDirs(d: String, pattern: String = "*"): List[String] =
    findFileOrDirRecursively(new File(d), pattern, true)

  def addFileSuffix(source: String, suffix: String): String = {
    val f = new java.io.File(source)
    s"${f.getPath}-${suffix}"
  }



  def getDataPath(groupIndices: Seq[String], inputDir: String): List[(List[Int], String)] = {
    def isLeafDir(groupIndices: Seq[String], path: String): Boolean =
    groupIndices.forall({f: String => path.contains("%s=".format(f))})

    def getValueFromPath(path: String): (List[Int], String) = {
      val group = path.split("/").takeRight(groupIndices.length)
        .map(_.split("=").toList)      // Array[List(featureName, value)]
        .map({case List(k, v) => (k, v.toInt)}) // Array[(featureName, value)]
        .toMap
      val featureValues = groupIndices.map({fname: String => group(fname)})
      (featureValues.toList, path)
    }

    org.apache.commons.io.FileUtils.listFilesAndDirs(new File(inputDir), new NotFileFilter(TrueFileFilter.INSTANCE), DirectoryFileFilter.DIRECTORY)
      .asScala
      .map({f: File => f.getCanonicalPath})
      .filter(isLeafDir(groupIndices, _))
      .map(getValueFromPath(_))
      .toList
  }
}

