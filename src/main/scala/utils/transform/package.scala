package utils

import scala.util.Random

package object transforms {
    def affineTransform[A](a: Double, b: Double)(implicit numeric: Numeric[A]): (A => Double) = {
        import numeric._
        t: A => a * t.toDouble + b
    }


    def affineTransformOrNone[A](a: Double, b: Double, nullMarker: A)(implicit integral: Integral[A]): (A => Option[Double]) = {
        import integral._
        t: A => {
            if (t == nullMarker)
                None
            else {
                val trans = affineTransform[Double](a, b)
                Some(trans(t.toDouble()))
            }
        }
    }

    def valueCap(minVal: Double , maxVal: Double): (Double => Option[Double]) = {t: Double =>
        if ((t >= minVal) && (t <= maxVal))
            Some(t)
        else
            None
    }


    def absChangeValue(
        minChange: Double, maxChange: Double,
        capBelow: Double = Double.MinValue,
        capAbove: Double = Double.MaxValue): Double => Double = {
        v: Double => {
            val t = v + minChange + Random.nextDouble * (maxChange - minChange)
            (t max capBelow) min capAbove
        }
    }

    def relChangeValue(r: Double): Double => Double = {v: Double => r * v}

}
