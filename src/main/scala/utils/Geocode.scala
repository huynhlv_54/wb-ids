package utils


import com.graphhopper.GraphHopper
import com.graphhopper.reader.osm.GraphHopperOSM
import com.graphhopper.routing.util.{ DataFlagEncoder, EdgeFilter, EncodingManager }
import java.lang.NullPointerException

/*
 * Convert osm file to the format used by GraphHopper
 * To run once on each node
 */
trait GHDataPreparer {
  val osmPath: String
  val graphhopperPath: String

  def convertMapFormat(): Unit = {
    val encoder = new DataFlagEncoder()
    val hopper = new GraphHopperOSM()
      .setStoreOnFlush(true)
      .setEncodingManager(new EncodingManager(encoder))
      .setDataReaderFile(osmPath)
      .setGraphHopperLocation(graphhopperPath)
    hopper.importOrLoad()
  }

  def main(args: Array[String]): Unit = {
    convertMapFormat()
  }

}

object GH_MichiganPreparer extends GHDataPreparer {
  val osmPath = "map/michigan/michigan-latest.osm.pbf" // input file
  val graphhopperPath = "map/graphhopper/data" // where gh will store data
}

// Using Graphhopper to do do map matching
object GH_Michigan_RoadTypeFinder {
  val graphhopperPath = "map/graphhopper/data" // where gh will store data
  val encoder = new DataFlagEncoder()
  val hopper = new GraphHopperOSM()
    .setStoreOnFlush(true)
    .setInMemory()
    .setAllowWrites(false) // reading is threadsafe, but writing is not
    .setEncodingManager(new EncodingManager(encoder)) //.setDataReaderFile(osmPath)
    .setGraphHopperLocation(graphhopperPath)
  hopper.load(graphhopperPath)
  val index = hopper.getLocationIndex


  /**
    From source code of DataFlagEncoder: The list of road type is as follows:
    reserve index=0 for unset roads (not accessible)
    "_default",
    "motorway", 
    "motorway_link",
    "motorroad", 
    "trunk", 
    "trunk_link",
    "primary", 
    "primary_link", 
    "secondary", 
    "secondary_link", 
    "tertiary", 
    "tertiary_link",
    "unclassified",
    "residential", 
    "living_street", 
    "service", 
    "road", 
    "track",
    "forestry", 
    "cycleway", 
    "steps", 
    "path", 
    "footway", 
    "pedestrian",
    "ferry", 
    "shuttle_train"
    );
    */
  def getRoadType: (Double, Double) => Short = (lat, lon) => {
    // TODO: Consider if we want to do filtering/snapping here to ensure
    // that the results are roadtype suitable for cars
    try {
      val queryRes = index.findClosest(lat, lon, EdgeFilter.ALL_EDGES)
      val edge = queryRes.getClosestEdge
      encoder.getHighway(edge).toShort
    } catch {
      case e: Throwable => {
        e.printStackTrace()
        Short.MaxValue
      }
    }
  }
}

