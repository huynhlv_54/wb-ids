package utils


import net.sf.geographiclib

object GeoCalc {
  // Using GeographicLib
  private val daMask = geographiclib.GeodesicMask.DISTANCE | geographiclib.GeodesicMask.AZIMUTH
  private val llMask = geographiclib.GeodesicMask.LATITUDE | geographiclib.GeodesicMask.LONGITUDE

  val DISTANCE_THRESHOLD = 0.01
  def getDistanceAndAzimuth: (Double, Double, Double, Double) => (Double, Double) =
    (lat1: Double, lon1: Double, lat2: Double, lon2: Double) => {
      val g = geographiclib.Geodesic.WGS84.Inverse(lat1, lon1, lat2, lon2, daMask)
      val distance = g.s12
      val azimuth = if (distance < DISTANCE_THRESHOLD) 0.0 // too close => azimuth unspecified
      else if (g.azi1 < 0.0) g.azi1 + 360.0                // change range [-180 -> 180] to [0, 360]
      else g.azi1
      (distance, azimuth)
    }

  /*
   * Move from current location forward and to the right
   * @param lat Current longitude
   * @param lon Current latitude
   * @param heading The original direction. Used to determine what is left/right, 
   * and what is forward/backward
   * @param moveForward
   * @param moveRight
   */
  def moveForwardAndRight: (Double, Double, Double, Double, Double) => (Double, Double) =
    (lat: Double, lon: Double, heading: Double, moveForward: Double, moveRight: Double) => {
      val angle = math.toDegrees(math.atan2(moveRight, moveForward))
      val s12 = math.sqrt(moveForward * moveForward + moveRight * moveRight)
      val g = geographiclib.Geodesic.WGS84.Direct(lat, lon, heading + angle, s12, llMask)

      (g.lat2, g.lon2)
    }

  /*
   * Move from current location forward and to the right
   * @param lat Current longitude
   * @param lon Current latitude
   * @param heading The original direction.
   * @param angle How much we want to change in direction
   * @param distance How far we want to move
   */
  def moveByAngle: (Double, Double, Double, Double, Double) => (Double, Double) =
    (lat: Double, lon: Double, heading: Double, angle: Double, distance: Double) => {
      val g = geographiclib.Geodesic.WGS84.Direct(lat, lon, heading + angle, distance, llMask)
      (g.lat2, g.lon2)
    }
}
