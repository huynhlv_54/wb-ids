package utils

import java.io.File
import org.apache.spark.sql.types.{ LongType, StructField, StructType }
import org.apache.spark.sql.{DataFrame, Row}
import org.apache.spark.sql.functions.{col, udf, max}
import scala.collection.concurrent.TrieMap
import scala.io.Source

case class LookupTable(elements: List[String], table: TrieMap[List[Int], Long]) {
  val total = table.values.sum
  print("Creating LookupTable for ")
  println(elements.mkString("-"))
}

object Detector extends SparkInit {

  /**
    *  Read profile from csv file
    *  Format of csv file:
    *  (header)  | f0 | f1 | ... |fn-1| freq|
    *  (row 0)
    *  (row 1)
    *  (...)
    */
  def profileToLut(pfile: String): LookupTable = {

    val source = Source.fromFile(pfile).getLines.toArray
    val features = source(0).split(",").map(_.trim).init.toList
    var LUT = new TrieMap[List[Int], Long]()
    source.tail.map(line => line.trim().split(",").map(_.trim).toList) // line -> row = array of cells
      .filter(row => row.filter(_.isEmpty()).length == 0) // ignore row with empty cells
      .foreach(row => {
        val featureVal = row.init.map(_.toInt)
        val freq = row.last.toLong
        LUT.update(featureVal, freq)
      })

    LookupTable(features, LUT)
  }


  /**
    *  Record the frequency of a message as observed by given features
    */
  def lookupFreq(LUT: LookupTable, msg: Row): Long = {
    val values: List[Int] = LUT.elements.map({f => msg.getAs[Int](f)}) // msg -> feature vector
    LUT.table.getOrElse(values, 0L)   // feature vector -> frequency
  }

  /**
    * Lookup the frequency of each feature (simple or compound)
    * 
    * @param LUTs
    * @param msg: Row
    * @param keepCols: the fields in the original message to keep
    */
  def lookupFreq(profile: Profile, msg: Row, keepCols: Seq[StructField]): Row = {
    val allFreqs = profile.map{LUT: utils.LookupTable => {
      // msg => feature vector
      val hasNull = LUT.elements.map(f => msg.isNullAt(msg.fieldIndex(f)))
        .exists(p => p)
      if (hasNull) {
        Long.MaxValue // indicating a null value
      } else {
        val fv = LUT.elements.map{f => msg.getAs[Int](f)}
        LUT.table.getOrElse(fv, 0L)
      }
    }}
    val keeps = keepCols.map(st => msg.getAs[AnyVal](st.name))
    Row.fromSeq(keeps ++ allFreqs)
  }

  /**
    * Create a DF of frequency
    * @param profiles: The set of profiles to be used
    * @param profileIndices: The fields used to select the suitable profile for each message
    * @param msgDF: The set of bucketized messages, which is the input for detection
    * @param keepCols: The column in msgDF that we want to keep (for future inspection)
    */
  def lookupFreqWithSpecializedProfiles(
    fallbackProfile: Profile,
    profiles: TrieMap[List[Int], Profile],
    profileIndices: Seq[String],
    msgDF: DataFrame,
    keepCols: Seq[StructField]
  ): DataFrame = {
    val freqs = msgDF.rdd.map {msg: Row => {
      // Select the suitable profile
      val i = profileIndices.map(field => msg.getAs[Int](field)).toList

      val selectedProfile = if (profiles.contains(i)) profiles(i) else fallbackProfile

      // Do the lookup, using this profile
      lookupFreq(selectedProfile, msg, keepCols)
    }}
    
    val schema = StructType(
      keepCols ++
        profiles.values.head.map{LUT => LUT.elements.mkString("-") }
        .map{fname => StructField(fname, LongType)}
    )
    spark.createDataFrame(freqs, schema)
  }


  def lookupFreq(
    profile: Profile,
    msgDF: DataFrame,
    keepCols: Seq[StructField]
  ): DataFrame = {
    val freqs = msgDF.rdd.map {msg: Row => lookupFreq(profile, msg, keepCols)}
    
    val schema = StructType(
      keepCols ++
        profile.map{LUT => LUT.elements.mkString("-") }
        .map{fname => StructField(fname, LongType)}
    )
    spark.createDataFrame(freqs, schema)
  }

  /**
    * Create a DF of frequency
    * @param profiles: The set of profiles to be used
    * @param profileIndices: The fields used to select the suitable profile for each message
    * @param msgDF: The set of bucketized messages, which is the input for detection
    * @param keepCols: The column in msgDF that we want to keep (for future inspection)
    */
  def lookupFreq(
    fallbackProfile: Profile,
    profiles: TrieMap[List[Int], Profile],
    profileIndices: Seq[String],
    msgDF: DataFrame,
    keepCols: Seq[StructField]
  ): DataFrame = if (profileIndices.isEmpty) lookupFreq(fallbackProfile, msgDF: DataFrame, keepCols)
  else lookupFreqWithSpecializedProfiles(fallbackProfile, profiles, profileIndices, msgDF, keepCols)


  /**
    *  Detect if the frequency of a msg is too low, given a LUT (corresponding to a feature)
    *  Note: msg must have been digitized
    */
  def detect(LUT: LookupTable, msg: Row, threshold: Int): (Boolean, String, Double) = {
    val freq = lookupFreq(LUT, msg)
    val score = math.log10(LUT.total / math.max(1, freq))
    if (freq < threshold) (true, LUT.elements.mkString("-"), score)
    else (false, "", score)
  }


  /**
    *  Detect if the frequency of a msg is too low
    *  Note: msg must have been digitized
    */
  def detect(LUTs: Seq[LookupTable], msg: Row, thresholds: Seq[Int]): (Boolean, Seq[String], Double) = {
    val res = (LUTs zip thresholds).map({case (l, t) => detect(l, msg, t)})
    val detected = res.map({case (verdict, reason, score) => verdict}).exists({v: Boolean => v})
    val reasons = res.map({case (verdict, reason, score) => reason})
    val score = res.map({case (verdict, reason, s) => s}).sum
    (detected, reasons, score)
  }

  def readProfile(profileDir: String, featureList: Seq[String]): Profile = {
    featureList.map{pName: String => profileDir + "/" + pName + ".csv"}  // get file name
      .filter{path: String => (new File(path)).exists()} // only available profiles
      .map{pfile: String => profileToLut(pfile)}
  }


  // groupBy: How to count detected result.
  // E.g.: groupBy = (RxDevice, TxRandom) => a message sequence of the same (RxDevice, TxRandom) is detected if
  // one message having this value is detected
  def freqToDetectResult(features: List[String], freqsDF: DataFrame, thresholds: List[Long], groupBy: List[String] = List()): DataFrame = {
    //Detection results, based on frequency and thresholds
    val udfs = features
      .zip(thresholds)
      .map{case (fname, threshold) => (fname,
        udf({x: Long => if (x < threshold) 1 else 0}))}
      .toMap

    var res = freqsDF
    for (fname <- udfs.keys) {
      res = res.withColumn(fname + "-detect", udfs.get(fname).get.apply(col(fname)))
        .drop(fname)
        .withColumnRenamed(fname + "-detect", fname)
    }

    // aggregate result by row
    res = res.withColumn("tempresult", udfs.keys.map(col).reduce((c1, c2) => c1 + c2))
      .withColumn("tempresult2", udf{t: Int => if (t > 0) 1 else 0}.apply(col("tempresult")))

    // aggregate result by group
    res = if (!groupBy.isEmpty)
      res.groupBy(groupBy.head, groupBy.tail :_*)
        .agg(max("tempresult2").alias("result"))
    else
      res.withColumnRenamed("tempresult2", "result")

    res.drop("tempresult")
  }


  def detectResultToRow(result: (Boolean, Seq[String], Double)): Row = {
    val detected = result._1
    val reasons = result._2.mkString(";")
    val score = result._3
    Row.fromTuple((detected, reasons), score)
  }


  def getFrequencyTable(attacks: DataFrame, profileList: List[List[String]], profileDir: String): (List[String], DataFrame) = {
    // read the list of profiles
    val featureNames = profileList.map(p => p.mkString("-"))
      .filter{fname: String => (new File(profileDir + "/" + fname + ".csv")).exists()}

    val profiles = readProfile(profileDir, featureNames)
    println("Done reading profiles")

    // Msg -> Frequency
    val colsToKeep = attacks.schema.fields.filter(s => spmd.Sequence.sequenceIndex.contains(s.name))
    val df = utils.Detector.lookupFreq(profiles, attacks, colsToKeep)
    (featureNames, df)
  }


}

