package utils

import org.apache.spark.sql.{DataFrame, SparkSession}

trait SparkInit {
  /** Spark session */
  lazy val spark = SparkSession.builder()
    .appName("Noname.app")
    .config("spark.master", "local[*]")
    .config("spark.local.dir", "data/spark_tmp")
    .config("spark.cleaner.referenceTracking.cleanCheckpoints", "true")
    .config("spark.ui.showConsoleProgress", "true")
    .getOrCreate()
  /** Spark context */
  lazy val sc = spark.sparkContext
  sc.setCheckpointDir("data/spark_checkpoint")

  def close(): Unit = spark.close()
}
