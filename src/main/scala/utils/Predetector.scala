package utils



import java.io.File
import org.apache.spark.sql.types.StructField
import scala.collection.concurrent.TrieMap

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.col

import utils.Detector.{lookupFreq, readProfile}


trait Predetector extends utils.SparkInit {
  val msgDF: DataFrame

  // List of feature names. In compound features, data elements are separated by "-"
  val featureList: Seq[String]

  // List of fields used to identify the suitable profiles
  // e.g., if we build separate Profiles for diferent road types, then indices = Seq[String]("RoadType")
  val indices: Seq[String] = List[String]()

  // The cols in the original messages that we don't want to drop
  val colsToKeep: Seq[StructField]

  // Attack file
  val raw_attacks: DataFrame

  // Directory containing specialized profiles
  val profileDir: String

  // Directory containing common profile, which is used as fallback if no
  // specialized profiles are applicable to the current messages
  val commonProfileDir: String

  val BUCKET_DEF: String
  val outputDir: String
  val isAtkInSeq: Boolean


  def getFrequencyTable(attacks: DataFrame): DataFrame = {
    // read the common profile
    val commonProfile = readProfile(commonProfileDir, featureList)

    // Read specialized profiles
    val profiles = new TrieMap[List[Int], Profile]()
    val canonicalProfileDir = (new File(profileDir)).getCanonicalPath
    if (!indices.isEmpty) utils.FileUtils.findSubDirs(canonicalProfileDir, ".*=[0-9]+").foreach(
      fullPath => {
        // check if the path follows the format
        println(fullPath)
        val profileName: Array[String] = fullPath.drop(canonicalProfileDir.length() + 1).split("/")
        println(s"profileName: ${profileName.toList}")
        val (indexKeys, indexValues) = profileName.map(kv => {
          val p = kv.split("=")
          (p(0), p(1).toInt)
        }).unzip
        if (indexKeys.toList.equals(indices.toList)) {
          val profile = readProfile(fullPath, featureList: Seq[String])
          profiles.update(indexValues.toList, profile)
        } // else: ignore this path
    })
    println("Done reading profiles")

    val df = {
      val _df = utils.Detector.lookupFreq(commonProfile, profiles, indices, attacks, colsToKeep)
      if (isAtkInSeq) _df.filter(col("changed") =!= 0) else _df
    }
    df
  }

  def main(args: Array[String]): Unit = {
    // From MSG -> Frequency
    val freqDF = getFrequencyTable(msgDF)

    // Save to disk and reload to get rid of the long SQL
    freqDF.write.format("csv").option("header", "true").mode("overwrite").save(outputDir)
  }
}
