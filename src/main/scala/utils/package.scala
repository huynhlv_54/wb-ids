package object utils {
  /**
    *  Generate a string for debugging memory usage
    */
  def meminfo: String = {
    val mb = 1024 * 1024
    val runtime = Runtime.getRuntime
    List(
      "** Used Memory:  " + (runtime.totalMemory - runtime.freeMemory) / mb,
      "** Free Memory:  " + runtime.freeMemory / mb,
      "** Total Memory: " + runtime.totalMemory / mb,
      "** Max Memory:   " + runtime.maxMemory / mb)
      .mkString("\n")
  }

  /**
    * Profile type
    */
  type Profile = Seq[LookupTable]
}
