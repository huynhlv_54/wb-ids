package utils

import scala.collection.mutable.ArrayBuffer
import scala.io.Source
import net.liftweb.json._

import org.apache.spark.sql._
import org.apache.spark.sql.functions.{count, col}
import org.apache.spark.sql.types.IntegerType
import org.apache.spark.ml.feature.Bucketizer

case class Bucket(feature: String, binning: List[Double])

object BucketDef {

  def extendedSplits(lo: Double, hi: Double, nbins: Int): Array[Double] = {
    val inc = (hi - lo) / nbins
    val buckets = new ArrayBuffer[Double](nbins + 2)
    buckets.append(Double.NegativeInfinity)
    for (i <- 0 to nbins) buckets.append(lo + inc * i)
    buckets.append(Double.PositiveInfinity)
    buckets.toArray
  }


  /**
    * Read bucketizers from a json file
    * @param jsonfile
    * @return Map {feature name => corresponding Bucketizer}
    */
  def readBucketSpecs(jsonfile: String, availableColumns: Array[String]): Bucketizer = {
    // json file to map
    implicit val formats = net.liftweb.json.DefaultFormats
    val availableFeatures = availableColumns.toSet

    val json = parse(Source.fromFile(jsonfile).mkString)
    val bucketSpecs = (for {
      JObject(objs) <- json
      obj <- objs
      JField(fname, jbucket) = obj
      JArray(bucket) = jbucket
      low = bucket(0).extract[Double]
      high = bucket(1).extract[Double]
      nbins = bucket(2).extract[Int]
    } //yield bucket).toList
    yield (fname, (low, high, nbins)))
      .toArray
      .filter{case (fname, _) => availableFeatures.contains(fname)}
      .unzip

    val inputCols: Array[String] = bucketSpecs._1
    val outputCols = inputCols.map(s => s + "-binned")
    val splitsArray = bucketSpecs._2.map{case (low, high, nbins) =>
      extendedSplits(low, high, nbins)}

    new Bucketizer()
      .setInputCols(inputCols)
      .setSplitsArray(splitsArray)
      .setOutputCols(outputCols)
      .setHandleInvalid("keep")
  }
}
