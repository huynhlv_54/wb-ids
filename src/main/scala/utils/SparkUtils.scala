package utils

import java.io.File

import org.apache.spark.sql.functions.{count, col, when, lit}
import org.apache.spark.sql.types.IntegerType
import org.apache.spark.sql.{DataFrame, SparkSession}


import utils.FileUtils.getListOfFiles
import utils.BucketDef.readBucketSpecs


object SparkUtils {
  /**
    * Save Spark DataFrame as one file
    * @param df
    * @param fileName
    * @param header
    */
  def saveDFAsOneFile(df: DataFrame, fileName: String, header: Boolean): Unit = {
    saveDFAsOneFile(df, "./", fileName, header)
  }

  /**
    *  Save Spark DataFrame as one file
    * @param df data frame
    * @param baseDir output directory
    * @param fileName file name
    * @param header option to include (true) or not include (false) csv header
    */
  def saveDFAsOneFile(df: DataFrame, baseDir: String, fileName: String, header: Boolean = true): Unit = {
    // Write to a directory.
    val tmpDirName = "%s/%s_tmp.csv".format(baseDir, fileName)
    df.repartition(1)
      .write
      .option("header", header.toString)
      .mode("overwrite")
      .csv(tmpDirName)

    val tmpDir = new File(tmpDirName)
    val tmpFile = getListOfFiles(tmpDirName).filter(_.getName.endsWith(".csv"))(0)
    val outFileName = if (fileName.endsWith(".csv")) fileName else fileName + ".csv"
    val finalFile = new File("%s/%s".format(baseDir, outFileName))
    tmpFile.renameTo(finalFile)
    tmpDir.listFiles.foreach(f => f.delete)
    tmpDir.delete
  }

  /**
    * Compute the histogram of a feature
    * @param ff feature
    * @param df dataset
    * @param baseDir output directory
    */
  def histogramming(ff: List[String], df: DataFrame, baseDir: String): DataFrame = {
    val res = histogramming(ff, df)
    saveDFAsOneFile(res, baseDir, ff.mkString("-"), true)
    res
  }

  /**
    * Compute the histogram of a feature
    * @param ff feature
    * @param df dataset
    * @return the histogram in the form of a data frame
    */
  def histogramming(ff: List[String], df: DataFrame): DataFrame = {
    println(ff)
    val res = df.groupBy(ff(0), ff.tail: _*)
      .agg(count(lit(1)).alias("freq"))
    if (ff.length == 1)
      res.orderBy(ff(0))
    else res
  }

  def filterOOR(df: DataFrame, bucketDefFile: String): DataFrame = {
    val bucketizer = readBucketSpecs(bucketDefFile, df.columns)
    val features = bucketizer.getInputCols
    val splitsArray = features.zip(bucketizer.getSplitsArray).toMap

    var res = df
    for (f <- features) {
      val splits = splitsArray.get(f).get
      res = res.filter(col(f) >= splits(1) && col(f) <= splits(splits.length - 2))
    }

    res
  }

  def bucketize(df: DataFrame, bucketDefFile: String): DataFrame = {
    val bucketizer = utils.BucketDef.readBucketSpecs(bucketDefFile, df.columns)
    val features = bucketizer.getInputCols
    val splitsArray = features.zip(bucketizer.getSplitsArray).toMap

    println(s"Bucketizing: ${features.toList}")
    var res = bucketizer.transform(df)

    // Remove "-binned" from feature name
    for (fname <- res.columns) {
      if (fname.endsWith("-binned")) {
        val name = fname.dropRight("-binned".length)
        res = res.drop(name)
          .withColumnRenamed(fname, name)
          .withColumn(name, col(name).cast(IntegerType))
      }
    }
    res
  }
}

