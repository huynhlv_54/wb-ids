package wydot
import org.apache.spark.sql.SparkSession


object Main {
    def main_(args: Array[String]): Unit = {
        val spark = SparkSession
            .builder()
            .appName("WYDOT-BSM")
            .config("spark.master", "local")
            .getOrCreate()

        var df = spark
            .read
            .format("csv")
            .option("inferSchema", "true")
            .option("header", "true")
            .load("data/WYDOT/WYDOT_BSM.csv")

        // Some fields not useful to us now.
        df = df.drop("metadata.logFileName")   // We don't have the original files
            .drop("metadata.payloadType")      // Always the same for BSM

        // discard BSM part II for now
        for (c: String <- df.columns) {
            if (c.contains("data.partII"))
                df = df.drop(c)
        }

        df.printSchema()
    }

}
