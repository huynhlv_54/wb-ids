package wydot

import org.apache.spark.sql.functions.col

object Json2csv extends utils.SparkInit {
    def main(args: Array[String]): Unit = {
        // sc.hadoopConfiguration.get("mapreduce.input.fileinputformat.input.dir.recursive")
        // load file recursively
        sc.hadoopConfiguration.set("mapreduce.input.fileinputformat.input.dir.recursive","true")
        val df = spark.read
            .format("json")
            .load("data/WYDOT/BSM/*/*/*/*/*")
            .select(  // flatten
                      //col("metadata.bsmSource"),
                //col("metadata.logFileName"),
                //col("metadata.odeReceivedAt"),
                col("metadata.recordGeneratedAt"),
                col("metadata.recordGeneratedBy"),
                // col("metadata.recordType"),
                // col("metadata.sanitized"),
                // col("metadata.schemaVersion"),
                // col("metadata.serialId.*"),
                col("metadata.validSignature"),
                col("payload.data.coreData.accelSet.*"),
                col("payload.data.coreData.accuracy.*"),
                col("payload.data.coredata.brakes.abs"),
                col("payload.data.coredata.brakes.auxBrakes"),
                col("payload.data.coredata.brakes.brakeBoost"),
                col("payload.data.coredata.brakes.scs"),
                col("payload.data.coredata.brakes.traction"),
                col("payload.data.coredata.brakes.wheelBrakes.*"),
                col("payload.data.coredata.heading"),
                col("payload.data.coredata.id"),
                col("payload.data.coredata.msgCnt"),
                col("payload.data.coredata.position.*"),
                col("payload.data.coredata.secMark"),
                col("payload.data.coredata.size.*"),
                col("payload.data.coredata.speed"),
                col("payload.data.coredata.transmission")
                // col("payload.data.partII"),  // Not considered for now
                // col("payload.dataType")
            )

        df.repartition(1).write
            .mode("overwrite")
            .format("csv")
            .option("header", "true")
            .save("data/WYDOT/BsmP1")
    }

}
