package spmd


import java.io.{ File, PrintWriter }
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.{col, abs}


object DataCleaner extends utils.SparkInit {
  def headers(): Array[String] = {
    val headerCSV = spark
      .read
      .format("csv")
      .option("header", "true")
      .load("data_slow/raw/spmd/bsm_head.csv")
    headerCSV.columns
  }

  def main(args: Array[String]): Unit = {
    val df = spark.read
      .format("csv")
      .option("inferSchema", "true")
      .option("header", false)
      .load("data_slow/raw/spmd/bsm/*.csv")
      .toDF(headers(): _*)
      .drop("PathCount", "RadiusofCurve", "Confidence")
      .filter(abs(col("Ay") - 20.01) >= 0.001)

    val columns = df.columns.mkString(",")
    val pw = new PrintWriter(new File("data/spmd/bsm/bsm_head_trimmed.csv" ))
    pw.write(columns)
    pw.close
    utils.SparkUtils.saveDFAsOneFile(df, "data/spmd/bsm/all", "all.csv", false)
  }
}
