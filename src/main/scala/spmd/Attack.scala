package spmd

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{DataFrame, Dataset, Row}
import org.apache.spark.sql.functions.{col, udf}

object Attack extends utils.SparkInit {
  import spark.implicits._

  def attack(
    df: DataFrame,
    outputDir: String,
    field: String,
    changer: Double => Double,
    changerName: String) : (DataFrame, String, String) = {
    
    // Change the messages
    val newCol = udf(changer).apply(col(field))
    val attackSet = df.withColumn(field, newCol)

    // Write changed messages to files
    val outFileName = "%s.csv".format(changerName)
    utils.SparkUtils.saveDFAsOneFile(attackSet, outputDir, outFileName, true)
    (attackSet, outputDir, outFileName)
  }

  private def seqSlice(sourceDir: String,
    bucketDef: String,
    margin: Int): RDD[Array[BSMP1]] = {
    val wSize = 2 * margin + 1

    // na.drop: Not consider messages that already contain bogus value.
    val df = utils.SparkUtils.filterOOR(
      spmd.DataSetUtil.readDF(sourceDir),
      bucketDef).na.drop()

    // Identify message sequences. Sequences are stored in DF
    val seqsInDf: DataFrame = Sequence.toSequenceDF(df)

    // Extract sequences from DF. Each sequence is an Array[BSMP1]
    val separatedSeqs: RDD[Array[Row]] = Sequence.separateSequences(seqsInDf)

    // Extract sequences of fixed size from variable-size sequence
    separatedSeqs.flatMap(seq => seq.sliding(wSize).filter(_.size == wSize))
      .map(s => s.map(Sequence.rowToBsm(_)))
  }

  private def renumberSeqs(atkSeqs: RDD[Array[BSMP1Mod]]): Dataset[BSMP1Mod] = {
    // Assign new (RxDevice, TxRandom) to distinguish the sequences,
    // and then mix them to a common dataset
    val atks: RDD[BSMP1Mod] = atkSeqs.zipWithIndex()
      .map{case (a, rnum) => a.map(bsmp1mod => {
        val bsmp1 = bsmp1mod.bsmp1
        val bsmp1WithNewSeq = bsmp1.copy(
          RxDevice = ((rnum >>> 32) & 0xffffffff).toInt,
          TxRandom = (rnum & 0xffffffff).toInt)
        new BSMP1Mod(bsmp1WithNewSeq, bsmp1mod.changed)
      })}  // RDD[Array[BsmP1Mod]], different sequences now have different (RxDevice, TxRandom)
      .flatMap(s => s)

    atks.toDS()
  }


  /**
    * Convert recorded messages to attacks
    * 
    * @param df Dataframe of BSMP1, with Gentime converted to seconds
    * @param wSize Number of messages in a sequence containing a modified message
    * @return A dataframe containing (modified messages) and (unmodified messsages surrounding the modified
    * ones).
    */
  def attackInSeq(
    sourceDir: String,
    bucketDef: String,
    cond: BsmSeq => Boolean,
    seqChanger: BsmSeq => BsmModSeq,
    margin: Int): Dataset[BSMP1Mod] = {
    val fixedSizeSeqs = seqSlice(sourceDir: String, bucketDef: String, margin: Int)
    
    // Generate attack sequences
    val atkSeqs: RDD[BsmModSeq] = fixedSizeSeqs
      .map(seq => if (cond(seq)) Some(seqChanger(seq)) else None)  // RDD[Option[BsmModSeq]]
      .flatMap(s => s)

    renumberSeqs(atkSeqs: RDD[Array[BSMP1Mod]])
  }

}
