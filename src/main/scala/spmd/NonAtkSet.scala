package spmd

/**
  * Select "clean" non-attack message, from a non-atk dataset
  */
trait NonAtkSet {
  val margin: Int
  val SOURCE_DIR: String
  val BUCKET_DEF: String = "BucketDef.json"
  lazy val OUT_DIR: String = utils.FileUtils.addFileSuffix(SOURCE_DIR, "cln")

  def main(args: Array[String]): Unit = {
    val wSize = 2 * margin + 1
    val df = utils.SparkUtils.filterOOR(
      spmd.DataSetUtil.readDF(SOURCE_DIR).na.drop(),
      BUCKET_DEF)

    df.write
      .format("csv")
      .option("header", "true")
      .save(OUT_DIR)
  }
}


object NonAtk_TS extends NonAtkSet {
  val margin = 12
  val SOURCE_DIR = "data/spmd/bsm/apr_test"
}

object NonAtk_TS_x001 extends NonAtkSet {
  val margin = 12
  val SOURCE_DIR = "data/spmd/bsm/apr_test/april_150.csv"
}
