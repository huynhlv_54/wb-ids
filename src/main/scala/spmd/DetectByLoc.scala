package spmd


import org.apache.spark.sql.{DataFrame, Row}
import org.apache.spark.sql.types.{BooleanType, StringType, StructField, StructType}
import utils.LookupTable
import utils.Detector.{detect, detectResultToRow, readProfile}
import utils.SparkUtils.saveDFAsOneFile

object DetectByLoc extends utils.SparkInit {

    def readProfiles(path: String, profileList: Seq[String]): Map[(Int, Int), Seq[LookupTable]] = {
        val pattern = "Latitude_(\\d+)_Longitude_(\\d+)$"
        val subDirs = utils.FileUtils.findSubDirs(path, pattern)

        subDirs.map{s: String => {
            // Get the latitude and longitude
            val m = pattern.r.findFirstMatchIn(s).get // Match
            val (lat, lon) = (m.group(1).toInt, m.group(2).toInt)
            println("Reading profiles %d, %d".format(lat, lon))

            // read the profile set for this (lat, lon)
            val profileSet = readProfile(s, profileList)

            ((lat, lon), profileSet)}}
            .toMap
    }

    def massDetect(profileSets: Map[(Int, Int), Seq[LookupTable]],
        atks: DataFrame, thresholds: List[Int], logFile: String): Unit = {
        val writer = new java.io.BufferedWriter(new java.io.FileWriter(new java.io.File(logFile)))
        val resAsRdd = atks.rdd.map({
            msg: Row => {
                val lat = msg.getAs[Int]("Latitude")
                val lon = msg.getAs[Int]("Longitude")
                profileSets.get((lat, lon)) match {
                    case Some(luts) => detectResultToRow(detect(luts, msg, thresholds))
                    case None => Row.fromTuple((false, "*ignore*"))
                }
            }
        })

        val res = spark.createDataFrame(
            resAsRdd,
            StructType(List(
                StructField("isDetected", BooleanType),
                StructField("reason", StringType))))
        saveDFAsOneFile(res, logFile, true)
    }


    def main(args: Array[String]): Unit = {
        if (args.length != 2) {
            println("Usage:")
            println("<spmd.DetectByLoc> <profileDir> <attackFiles>")
        }
        val profileDir = args(0)
        val attackDir = args(1)

        // Get the list of profiles
        val  profileList = List(
            //"Ax-Axy",
            "Ax-Ay-Az",
            "Ax-Ay",
            "Ax-Az",
            "Ax-YawRate",
            "Ax",
            //"Axy",
            //"Ay-Axy",
            "Ay-Az",
            "Ay-YawRate",
            "Ay",
            //"Az-Axy",
            "Az-YawRate",
            "Az",
            // "Elevation-Ax",
            // "Elevation-Axy",
            // "Elevation-Ay",
            // "Elevation-Az",
            // "Elevation-Heading",
            // "Elevation-Speed",
            // "Elevation-YawRate",
            // "Elevation",
            // "Heading-Ax-Ay",
            // "Heading-Ax-YawRate",
            // "Heading-Ax",
            // "Heading-Axy",
            // "Heading-Ay-YawRate",
            // "Heading-Ay",
            // "Heading-Az",
            // "Heading-YawRate",
            // "Heading",
            // "Latitude-Ax",
            // "Latitude-Axy",
            // "Latitude-Ay",
            // "Latitude-Az",
            // "Latitude-Elevation",
            // "Latitude-Heading",
            // "Latitude-Longitude-Elevation",
            // "Latitude-Longitude",
            // "Latitude-Speed",
            // "Latitude-YawRate",
            // "Latitude",
            // "Longitude-Ax",
            // "Longitude-Axy",
            // "Longitude-Ay",
            // "Longitude-Az",
            // "Longitude-Elevation",
            // "Longitude-Heading",
            // "Longitude-Speed",
            // "Longitude-YawRate",
            // "Longitude",
            "Speed-Ax-Ay",
            "Speed-Ax",
            //"Speed-Axy",
            "Speed-Ay",
            "Speed-Az",
            // "Speed-Heading-Ax",
            // "Speed-Heading-Ay",
            // "Speed-Heading-Az",
            // "Speed-Heading-YawRate",
            // "Speed-Heading",
            "Speed-YawRate",
            "Speed",
            //"YawRate-Axy",
            "YawRate"
        )
        val profileSets = readProfiles(profileDir, profileList)
        println("Set of profiles:")
        println(profileSets.keys)

        // Get the list of attacks file
        val atkFiles = utils.FileUtils.findFilesRecursively(attackDir, ".csv$")
        for (atkFile <- atkFiles) {
            val logFile = atkFile + ".detect"
            val attacks = spark.read
                .format("csv")
                .option("inferSchema", "true")
                .option("header", "true")
                .load(atkFile)

            val thresholds = (0 until profileList.length).map(_ => 1).toList
            massDetect(profileSets,
                attacks, thresholds, logFile)
        }

    }
}
