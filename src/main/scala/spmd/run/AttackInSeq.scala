package spmd.run

import java.io.File
import java.util.Calendar
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{ Dataset, Row }
import spmd.{BSMP1, BSMP1Mod, BsmSeq, Sequence, BsmModSeq}
import utils.SparkInit
import utils.SparkUtils.saveDFAsOneFile

import org.apache.spark.sql.DataFrame


/**
  * Note: Although this object use Spark, it should be used locally only.
  * Reason: We use Spark to quickly read csv, and then convert the DataFrame to Array.
  * Lots of shuffle will happen if we use this in a distributed setting.
  */
trait AttackInSeq {

  val SOURCE_DIR: String // original BSMP1 messages; To be defined by instance
  def OUT_DIR = utils.FileUtils.addFileSuffix(SOURCE_DIR, "atk")
  val BUCKET_DEF: String // used to remove out-of-range data
  val changer: BsmSeq => BsmModSeq
  val chooser: BsmSeq => Boolean
  val margin: Int
  val changerName: String


  def main(args: Array[String]): Unit = {
    
    val atkset = spmd.Attack.attackInSeq(SOURCE_DIR, BUCKET_DEF,
      chooser, changer, margin)

    val df = atkset.select("BsmP1.*", "changed").toDF
    println(s"Saving ${OUT_DIR}/m${margin}/${changerName}.csv")
    utils.SparkUtils.saveDFAsOneFile(df, s"${OUT_DIR}/m${margin}", s"${changerName}.csv", true)
  }
}


trait FCWChooser {
  val chooser: BsmSeq => Boolean = seq => {
    val msg = seq(seq.length / 2)
    msg.Speed >= 11.4 && msg.Speed <= 30.0
  }
}


object GradualSpeedTo0 {
  def getChanger: (Int, Int) => BsmSeq => BsmModSeq = (l, r) => seq => {
    val mid = seq.length / 2
    val stepL = seq(l).Speed / (mid - l + 1)
    val stepR = seq(r).Speed / (r - mid + 1)

    seq.zipWithIndex.map{case (bsmp1, i) =>
      if (i < l || i > r)
        BSMP1Mod(seq(i), 0)
      else if (i <= mid)
        BSMP1Mod(seq(i).copy(Speed = stepL * (mid - i)), 1)
      else
        BSMP1Mod(seq(i).copy(Speed = stepR * (i - mid)), 1)
    }
  }

  def getSymmetricChanger: Int => BsmSeq => BsmModSeq = radius => seq => {
    val mid = seq.length / 2
    getChanger(mid - radius, mid + radius)(seq)
  }

  def getAdaptiveChanger: BsmSeq => BsmModSeq = seq => {
    val range = rangeToChange(seq)
    if (range._1 == -1) Array()
    else getChanger(range._1, range._2)(seq)
  }

  def rangeToChange: BsmSeq => (Int, Int) = seq => {
    val accel = 4.5 // m / s^2
    val mid = seq.length / 2
    val acceptableAccels = seq.zipWithIndex.map{case (bsmp1, i) => 
      (bsmp1.Speed * 10 / (math.abs(mid - i) + 1) <= accel, i)
    }.filter{ case (a, i) => a }
      .map{case (a, i) => i}
    val l = acceptableAccels.filter(i => i < mid)
    val r = acceptableAccels.filter(i => i > mid)

    if (!l.isEmpty && !r.isEmpty) (l.last, r.head)
    else (-1, -1)
  }
}

trait SpeedTo0 {
  val changerName: String = "speed_to_0"
  val changer = GradualSpeedTo0.getSymmetricChanger(0)
}

trait NoAtk {
  val chooser: BsmSeq => Boolean = seq => true
  val changer: BsmSeq => BsmModSeq = seq => seq.map(bsmp1 => BSMP1Mod(bsmp1, 0))
  val changerName: String = "nonatk"
}

trait Margin3 {
  val margin = 3
}

trait NoMargin {
  val margin = 0
}

trait SpeedTo0_RX {
  val changerName = "SpeedTo0_RX"
  val changer = GradualSpeedTo0.getAdaptiveChanger
}


trait TestTiny {
  val SOURCE_DIR = "data/spmd/bsm/a_small_pieces"
}

trait TestSet_01 {
  val SOURCE_DIR = "data/spmd/bsm/test/x09.csv"
}

trait TestSet {
  val SOURCE_DIR = "data/spmd/bsm/test"
}

trait AtkSet {
  val SOURCE_DIR = "data/spmd/bsm/a_x20_limit_3000000"
}

// Attacks that change Speed to 0
// Attack in sequence
object Atk_TS_Seq extends AttackInSeq
    with AtkSet with CommonBucket
    with FCWChooser with SpeedTo0 with Margin3

// Attack: No sequences
object Atk_TS_NoSeq extends AttackInSeq
    with AtkSet with CommonBucket
    with FCWChooser with SpeedTo0 with NoMargin


// Attack: Simulate braking behavior
// Report false messages with false Ax and Speed
trait SimulatedAccel {
  val ax: Double
  val maxSpeed: Double = Double.MaxValue
  val minSpeed: Double = 0.0
  val deltaTime: Double = 0.1 // 10Hz

  val changer: BsmSeq => BsmModSeq = seq => {
    val s0 = seq.head.Speed
    val speeds = (0 until seq.length).map(i => s0 + i * ax * deltaTime)
    seq.zip(speeds).map { case (bsm: BSMP1, s: Double) => BSMP1Mod(bsm.copy(Speed = s, Ax = ax), 1)}
  }
}


// Attack: Report a position that is
// +x meters in front of the real position
// +y meters to the right of the real position
abstract class PosChange extends Serializable {
  val moveForward: Double
  val moveRight: Double

  val bsmChanger: BSMP1 => BSMP1 = bsm => {
    val (lat, lon) = utils.GeoCalc.moveForwardAndRight(bsm.Latitude, bsm.Longitude, bsm.Heading, moveForward, moveRight)
    bsm.copy(Latitude = lat, Longitude = lon)
  }

  def getChanger: BsmSeq => BsmModSeq = seq =>
  seq.map(bsm => BSMP1Mod(bsmChanger(bsm), 1))
}

// Attacks that change Speed to 0
// Attack in sequence
// Comment: With our detectors, this attack can be hard to detect
object Atk_TS_Seq_Position extends AttackInSeq
    with AtkSet with CommonBucket
    with FCWChooser with Margin3 {
  object posChanger extends PosChange {
    val moveForward = -20.0
    val moveRight = 0.0
  }
  val changer = posChanger.getChanger
  val changerName = "Back20m"
}
