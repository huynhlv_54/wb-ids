package spmd.run

import utils.SparkInit
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.{udf, col}

trait RoadTypeFinder extends Serializable {
  val source: String
  val dest: String
  val sourceHasHeader: Boolean = false

  // val source = "data/spmd/bsm/a_small_pieces"
  // val dest = "data/spmd/bsm/a_small_pieces_r"

  // use barefoot
  //lazy val rf = new utils.RoadFinder("./map/michigan/michigan.bfmap")

  //use GraphHopper
  @transient lazy val rf = utils.GH_Michigan_RoadTypeFinder
  def getRoadType(df: DataFrame): DataFrame = {
    val getRoadType: (Double, Double) => Short = (lat, lon) => rf.getRoadType(lat, lon)
    df.withColumn("RoadType", udf(getRoadType).apply(col("Latitude"), col("Longitude")))
  }

  def addRoadType(source: String, des: String): Unit = {
    val df = spmd.DataSetUtil.readDF(source, sourceHasHeader.toString())
    val df2 = getRoadType(df)
    df2.write.format("csv").option("header", "true").mode("overwrite").save(des)
  }

  def main(args: Array[String]): Unit = {
    addRoadType(source, dest)
  }
}

/*
object AddRoadType_x20 extends RoadTypeFinder {
  val source = "data/spmd/bsm/a_x20"
  val dest = "data/spmd/bsm/a_x20_r"
}

object AddRoadType_x80 extends RoadTypeFinder {
  val source = "data/spmd/bsm/a_x80"
  val dest = "data/spmd/bsm/a_x80_r"
}

 */
object AddRoadType_tiny extends RoadTypeFinder {
  val source = "data/spmd/bsm/a_small_pieces"
  val dest = "data/spmd/bsm/a_small_pieces_r"
}


object AddRoadType_Atkset extends RoadTypeFinder {
  override val sourceHasHeader = true
  val source = "data/spmd/bsm/atkset/speed_to_0/m3"
  val dest = "data/spmd/bsm/atkset_r/speed_to_0/m3"
}
