package spmd.run

import java.time.{ZoneId, ZonedDateTime}

import org.apache.spark.sql.functions.{col, udf, count, lit}
import utils.SparkInit

object Profile_x001 extends Profiler with ExtendedFeatureSet {
  val SOURCE_DATA = "data/spmd/bsm/a_small_pieces/i000.csv"
  val BUCKET_SPECS = "BucketDef.json"
  val HIST_DIR = "res/spmd/bsm/p_small"
}


object Profile_TrainingSet extends Profiler with ExtendedFeatureSet {
  val SOURCE_DATA = "data/spmd/bsm/a_x80"
  val BUCKET_SPECS = "BucketDef.json"
  val HIST_DIR = "res/spmd/bsm/p_x80"
}

object Profile_TrainingSet_cont extends Profiler with ExtendedFeatureSet {
  val SOURCE_DATA = "data/spmd/bsm/a_x80"
  val BUCKET_SPECS = "BucketDef.json"
  val HIST_DIR = "res/spmd/bsm/p_x80"

  override def main(args: Array[String]): Unit = {
    // binData()
    val dfBinned = spark.read.format("csv")
      .option("header", "true")
      .option("inferSchema", "true")
      .load(BINNED_DATA_DIR)
    spmd.Profiling.doProfile(features, dfBinned, HIST_DIR)
  }
}


// Simple profiles which consider only "Speed, Ax, Ay, YawRate"
trait ProfileDayNight extends SparkInit {
  val SOURCE_DATA: String // = "data/spmd/bsm/a_x80"
  val BUCKET_SPECS: String //  = "BucketDef.json"
  val HIST_DIR: String // = "res/spmd/bsm/p_x80_day_night"
  val BINNED_DIR: String

  val getHour: Long => Int = ts => {
    val sec = ts / 1000000.0 - 35.0 + 1072933200.0
    val msec = (sec * 1000).toLong
    val instant = java.time.Instant.ofEpochMilli(msec)
    val zonedInstant = ZonedDateTime.ofInstant(instant, ZoneId.of("GMT"))
    val hour = zonedInstant.getHour
    hour
  }

  // 0: night, 1: day
  def getDayNight: Int => Int = h => if (h >=5 && h <= 15) 0 else 1

  def main(args: Array[String]): Unit = {

    val hour = udf(getHour).apply(col("Gentime"))
    val dayNight = udf(getDayNight).apply(col("Hour"))
    val _df = spmd.DataSetUtil.readExtraFeatures(SOURCE_DATA)
      .withColumn("Hour", hour)
      .withColumn("DayNight", dayNight)
    val toKeep = _df.columns.filter(s => !s.startsWith("lead") && !s.startsWith("lag") && !s.startsWith("Diff"))
    val df = _df.select(toKeep.head, toKeep.tail: _*)

    val features = spmd.FeatureDef.allFeatures.filter(s =>
      s.length <= 2 &&
        !s.exists(datafield => datafield.startsWith("lead") || datafield.startsWith("lag"))
    )

    val _res = df.groupBy(col("Hour")).agg(count(lit(1)))
    utils.SparkUtils.saveDFAsOneFile(_res, HIST_DIR, "daynight.csv", true)

    val df_binned = utils.SparkUtils.bucketize(df, BUCKET_SPECS)
    // df_binned.write.partitionBy("DayNight")
    //   .format("csv").option("header", "true").mode("overwrite").save(BINNED_DIR)
    for (i <- 0 to 1) {
      // val df_i = spark.read.format("csv").option("header", "true").option("inferSchema", "true")
      //   .load(BINNED_DIR + s"/DayNight=$i").persist()
      val df_i = df_binned.filter(col("DayNight") === i).persist()
      spmd.Profiling.doProfile(features, df_i, s"${HIST_DIR}/$i")
    }
  }
}

object ProfileDayNight_x80 extends ProfileDayNight {
  val SOURCE_DATA: String = "data/spmd/bsm/a_x80"
  val BUCKET_SPECS: String = "BucketDef.json"
  val HIST_DIR: String = "res/spmd/bsm/p_x80_day_night"
  val BINNED_DIR: String = "data/spmd/bsm/x80_day_night_binned"
}

object ProfileDayNight_x20 extends ProfileDayNight {
  val SOURCE_DATA: String = "data/spmd/bsm/a_x20"
  val BUCKET_SPECS: String = "BucketDef.json"
  val HIST_DIR: String = "res/spmd/bsm/p_x20_day_night"
  val BINNED_DIR: String = "data/spmd/bsm/x20_day_night_binned"
}

object SplitDayNight_x20 extends ProfileDayNight {
  val SOURCE_DATA: String = "data/spmd/bsm/a_x20"
  val BUCKET_SPECS: String = "BucketDef.json"
  val HIST_DIR: String = "res/spmd/bsm/p_x20_day_night"
  val BINNED_DIR: String = "data/spmd/bsm/x20_day_night_binned"
  val SPLIT_DIR = "data/spmd/bsm/a_x20_split_day_night"

  override def main(args: Array[String]): Unit = {
    val hour = udf(getHour).apply(col("Gentime"))
    val dayNight = udf(getDayNight).apply(col("Hour"))
    val _df = spmd.DataSetUtil.readDF(SOURCE_DATA)
      .withColumn("Hour", hour)
      .withColumn("DayNight", dayNight)
      .drop("Hour")
      .write.format("csv").option("header", "true").partitionBy("DayNight").save(SPLIT_DIR)
  }
}



object Profile_small_r extends Profiler with ExtendedFeatureSetWithFileHeader {
  val SOURCE_DATA = "data/spmd/bsm/a_small_r.csv"
  val BUCKET_SPECS = "BucketDef.json"
  val HIST_DIR = "res/spmd/bsm/p_small_r"
  override val groupIndices = List("RoadType")
}


object Profile_Testset_r extends Profiler with ExtendedFeatureSetWithFileHeader {
  val SOURCE_DATA = "data/spmd/bsm/a_x20_r"
  val BUCKET_SPECS = "BucketDef.json"
  val HIST_DIR = "data/spmd/bsm/p_x20_r"
  override val groupIndices = List("RoadType")
}


object Profile_TrainingSet_r extends Profiler with ExtendedFeatureSetWithFileHeader {
  val SOURCE_DATA = "data/spmd/bsm/a_x80_r"
  val BUCKET_SPECS = "BucketDef.json"
  val HIST_DIR = "data/spmd/bsm/p_x80_r"
  override val groupIndices = List("RoadType")
}
