package spmd.run

import org.apache.spark.sql.types.StructField
import scala.collection.concurrent.TrieMap

import scala.language.postfixOps

import org.apache.spark.sql.{DataFrame, functions}
import org.apache.spark.sql.functions.{col, udf, array}
import utils.Detector.{lookupFreq, readProfile}
import utils.Profile

import utils.Predetector

object ArrayUtils {
  def concat(t1: Array[Array[Long]], t2: Array[Array[Long]]): Array[Array[Long]] = {
    t1.zip(t2).map{case (part1, part2) => part1 ++ part2}
  }
}

object EQ1_Thresholds {
  val thresholdPerFeature : Array[Long] = {
    val length = 40
    val res = Array.ofDim[Long](length)
    res(0) = 1L
    res(1) = 2L
    for (i <- 2 until length) {
      res(i) = res(i - 1) + res(i - 2)
    }
    res
  }
  def nfolds(nfeatures: Int): Array[Array[Long]] = {
    thresholdPerFeature.map(t => Array.fill(nfeatures)(t))
  }
  val threshold_array = thresholdPerFeature.map(t => Array(t))
}


object NeededFeatures {
  val featureNames = List("Speed", "Ax", "Ay", "YawRate",
    "Speed-Ax", "Speed-Ay", "Speed-YawRate", "Ax-Ay", "Ax-YawRate", "Ay-YawRate",
    "Speed-DeducedSpeed_0_1", "Heading-Azimuth_0_1",
    "Speed-DeducedSpeed_1_1", "Heading-Azimuth_1_1",
    "Speed-DeducedSpeed_2_2", "Heading-Azimuth_2_2",
    "Speed-DeducedSpeed_3_3", "Heading-Azimuth_3_3")
}

trait SPMD_Predetector extends Predetector {
  val sequenceIndex = spmd.Sequence.sequenceIndex
  lazy val msgDF = utils.SparkUtils.bucketize(spmd.DataSetUtil.readExtraFeatures(raw_attacks), BUCKET_DEF)
  lazy val colsToKeep: Seq[StructField] = msgDF.schema.fields.filter(s => spmd.Sequence.sequenceIndex.contains(s.name)
      || s.name.equals("Gentime")
      || s.name.contains("changed")
  )
}

object Predetector_Testset extends SPMD_Predetector with CommonBucket {
  val attackFile: String = "data/spmd/bsm/test"
  val raw_attacks = spmd.DataSetUtil.readDF(attackFile)

  val featureList: List[String] = NeededFeatures.featureNames
  val commonProfileDir = "data/spmd/bsm/p_x80"
  val profileDir = "data/spmd/bsm/p_x80" // not needed
  val outputDir: String = "data/spmd/bsm/test-freq"
  val isAtkInSeq: Boolean = false
}

object Predetector_Testset_r extends SPMD_Predetector with CommonBucket {
  override val indices = List[String]("RoadType")
  val attackFile: String = "data/spmd/bsm/a_x20_r"
  val raw_attacks = spmd.DataSetUtil.readDF(attackFile, "true")

  val featureList: List[String] = NeededFeatures.featureNames
  val commonProfileDir = "data/spmd/bsm/p_x80"
  val profileDir: String = "data/spmd/bsm/p_x80_r"
  val outputDir: String = "data/spmd/bsm/test-freq_r"
  val isAtkInSeq: Boolean = false
}


object Predetector_Tinyset extends SPMD_Predetector with CommonBucket {
  val attackFile: String = "data/spmd/bsm/a_small_pieces/"
  val raw_attacks = spmd.DataSetUtil.readDF(attackFile)

  val featureList: List[String] = NeededFeatures.featureNames
  val commonProfileDir: String = "data/spmd/bsm/p_x80"
  val profileDir: String = ""
  val outputDir: String = "data/spmd/bsm/tiny-freq"
  val isAtkInSeq: Boolean = false
}

object Predetector_Tinyset_r extends SPMD_Predetector with CommonBucket {
  override val indices = List[String]("RoadType")
  val attackFile: String = "data/spmd/bsm/a_small_pieces_r"
  val raw_attacks = spmd.DataSetUtil.readDF(attackFile, "true")
  val featureList: List[String] = NeededFeatures.featureNames
  val commonProfileDir: String = "data/spmd/bsm/p_x80"
  val profileDir: String = "data/spmd/bsm/p_x80_r"
  val outputDir: String = "data/spmd/bsm/tiny-freq_r"
  val isAtkInSeq: Boolean = false
}


object Predetector_AtkSet_NoSeq extends SPMD_Predetector with CommonBucket {
  val attackFile: String = "data/spmd/bsm/atkset/speed_to_0/m0"
  val raw_attacks = spark.read
    .format("csv")
    .option("inferSchema", "true")
    .option("header", "true")
    .load(attackFile)

  val featureList = NeededFeatures.featureNames
  val commonProfileDir: String = "data/spmd/bsm/p_x80"
  val profileDir: String = ""
  val outputDir: String = "data/spmd/bsm/atkset-freq/speed_to_0/m0"
  val isAtkInSeq: Boolean = true
}


object Predetector_AtkSet_Seq extends SPMD_Predetector with CommonBucket {
  val attackFile: String = "data/spmd/bsm/atkset/speed_to_0/m3"
  val raw_attacks = spark.read
    .format("csv")
    .option("inferSchema", "true")
    .option("header", "true")
    .load(attackFile)

  val featureList = NeededFeatures.featureNames
  val commonProfileDir: String = "data/spmd/bsm/p_x80"
  val profileDir = ""
  val outputDir: String = "data/spmd/bsm/atkset-freq/speed_to_0/m3"
  val isAtkInSeq: Boolean = true
}


// _r: with road type
object Predetector_AtkSet_Seq_r extends SPMD_Predetector with CommonBucket {
  override val indices = List("RoadType")
  val attackFile: String = "data/spmd/bsm/atkset_r/speed_to_0/m3"
  val raw_attacks = spark.read
    .format("csv")
    .option("inferSchema", "true")
    .option("header", "true")
    .load(attackFile)

  val featureList = NeededFeatures.featureNames
  val commonProfileDir: String = "data/spmd/bsm/p_x80"
  val profileDir = "data/spmd/bsm/p_x80_r"
  val outputDir: String = "data/spmd/bsm/atkset-freq_r/speed_to_0/m3"
  val isAtkInSeq: Boolean = true
}


trait Detector {
  val features: List[String]
  val detectorName: String = this.getClass.getSimpleName.replace("$", "")
  val thresholds: Array[Array[Long]]

  // Detection for different thresholds
  def doDetect(freqDF: DataFrame, outputDir: String, groupMsg: List[String] = List()): Unit = {
    for ((threshold, index) <- thresholds.zipWithIndex) {
      // resMsg: Detection results for each sequence
      val res = utils.Detector.freqToDetectResult(features, freqDF, threshold.toList, groupMsg)

      // Count the number of sequences, number of detected/not detected/nodecision sequences
      val summary = res.select(functions.sum("result"))
      utils.SparkUtils.saveDFAsOneFile(summary, outputDir + s"/${detectorName}",
        s"$index-summary.csv", true)
    }
  }
}


trait DetectorSet extends utils.SparkInit {
  val freqDFLoc: String
  val detectors: List[Detector]
  val outputDir: String
  val groupMsgBy: List[String] = List() // how to count number of detected instances
                                        // empty list => one msg = one instance
                                        // (RxDevice, TxRandom) => message sequence identified by the keys are one instance

  def main(args: Array[String]): Unit = {
    val freqDF = spark.read.format("csv").option("header", "true").option("inferSchema", "true").load(freqDFLoc).persist()

    for (d <- detectors) {
      d.doDetect(freqDF, outputDir, groupMsgBy)
    }
  }
}

object D1 extends Detector {
  val features = List("Speed", "Ax", "Ay", "YawRate")
  val thresholds = EQ1_Thresholds.nfolds(features.length)
}

object D2 extends Detector {
  val features = List("Speed-Ax", "Speed-Ay", "Speed-YawRate", "Ax-Ay", "Ax-YawRate", "Ay-YawRate")
  val thresholds = EQ1_Thresholds.nfolds(features.length)
}

object D3 extends Detector {
  val features = List("Speed-DeducedSpeed_0_1", "Heading-Azimuth_0_1")
  val thresholds = EQ1_Thresholds.nfolds(features.length)
}

object D4 extends Detector {
  val features = List("Speed-DeducedSpeed_1_1", "Heading-Azimuth_1_1")
  val thresholds = EQ1_Thresholds.nfolds(features.length)
}

object D5 extends Detector {
  val features = List("Speed-DeducedSpeed_2_2", "Heading-Azimuth_2_2")
  val thresholds = EQ1_Thresholds.nfolds(features.length)
}

object D6 extends Detector {
  val features = List("Speed-DeducedSpeed_3_3", "Heading-Azimuth_3_3")
  val thresholds = EQ1_Thresholds.nfolds(features.length)
}


trait AllFeaturesDetectors extends DetectorSet {
  val detectors = List(
    D1, D2, // Already done
    D3, D4, D5, D6)
}

object DetectorSet_NonAtk extends AllFeaturesDetectors {
  val freqDFLoc: String = "data/spmd/bsm/test-freq"
  val outputDir: String = "data/spmd/bsm/test-detect"
}


object DetectorSet_NonAtk_r extends AllFeaturesDetectors {
  val freqDFLoc: String = "data/spmd/bsm/test-freq_r"
  val outputDir: String = "data/spmd/bsm/test-detect_r"
}


object DetectorSet_NonAtk_Tiny extends AllFeaturesDetectors {
  val freqDFLoc: String = "data/spmd/bsm/tiny-freq"
  val outputDir: String = "data/spmd/bsm/tiny-detect"
}

object DetectorSet_NonAtk_Tiny_r extends AllFeaturesDetectors {
  val freqDFLoc: String = "data/spmd/bsm/tiny-freq_r"
  val outputDir: String = "data/spmd/bsm/tiny-detect_r"
}


// TODO: Add groupMsgBy and update Detector.doDetect
// to use this and do not drop rows with "changed" == 0
// (Do these two actions at the same time)
object DetectorSet_Atk_Seq extends AllFeaturesDetectors {
  val freqDFLoc: String = "data/spmd/bsm/atkset-freq/speed_to_0/m3"
  val outputDir: String = "data/spmd/bsm/atkset-detect/speed_to_0/m3"
}


object DetectorSet_Atk_Seq_r extends AllFeaturesDetectors {
  val freqDFLoc: String = "data/spmd/bsm/atkset-freq_r/speed_to_0/m3"
  val outputDir: String = "data/spmd/bsm/atkset-detect_r/speed_to_0/m3"
}

object DetectorSet_Atk_NonSeq extends AllFeaturesDetectors {
  val freqDFLoc: String = "data/spmd/bsm/atkset-freq/speed_to_0/m0"
  val outputDir: String = "data/spmd/bsm/atkset-detect/speed_to_0/m0"
}




// Predetector: for changing location
object Predetector_AtkSet_Seq_Location extends SPMD_Predetector with CommonBucket {
  val attackFile: String = "data/spmd/bsm/atkset/back_20_m/m3"
  val raw_attacks = spark.read
    .format("csv")
    .option("inferSchema", "true")
    .option("header", "true")
    .load(attackFile)

  val featureList = NeededFeatures.featureNames
  val commonProfileDir: String = "data/spmd/bsm/p_x80"
  val profileDir = ""
  val outputDir: String = "data/spmd/bsm/atkset-freq/back_20_m/m3"
  val isAtkInSeq: Boolean = true
}


object DetectorSet_Atk_Seq_Position extends AllFeaturesDetectors {
  val freqDFLoc: String = "data/spmd/bsm/atkset-freq/back_20_m/m3"
  val outputDir: String = "data/spmd/bsm/atkset-detect/back_20_m/m3"
  override val groupMsgBy = List("RxDevice", "TxRandom")
}
