package spmd.run

import org.apache.spark.sql.functions.col
import org.apache.spark.sql.DataFrame
import org.apache.spark.storage.StorageLevel

import utils.SparkInit

trait Profiler extends SparkInit {
  spark.conf.set("spark.app.name", "SPMD - Profiling")
  spark.conf.set("spark.default.parallelism", 2001)
  //spark.conf.set("spark.sql.files.maxPartitionBytes", 134217728 / 10)
  // 128M / 10: 1/10 of default
  // Reason: the raw data file grows when more features are added

  val SOURCE_DATA: String
  val BUCKET_SPECS: String
  val HIST_DIR: String
  val groupIndices: Seq[String] = List[String]()
  val features: List[List[String]] = spmd.FeatureDef.allFeatures.filter(s => s.length <= 5)


  def BINNED_DATA_DIR = {
    val r = if (SOURCE_DATA.endsWith("/")) SOURCE_DATA.dropRight(1) else SOURCE_DATA
    "ssd_cache/" + (if (r.endsWith(".csv")) r.dropRight(4) else r) + "-binned"
  }

  def readMyData(source: String): DataFrame

  // TODO: Optimization:
  // Instead of computing lead_X, lag_X then bucketize (X, lead_X, lag_X)
  // it is faster to bucketize X, then create bucketized lead_X, lag_X
  def binData(): Unit = {
    // read data
    val df = readMyData(SOURCE_DATA)

    // Remove unused columns to make the process faster
    val cols = df.columns.filterNot(s =>
      s.equals("FileID") || s.equals("Session") ||
        s.equals("RxDevice") || s.equals("TxDevice") ||
        s.equals("TxRandom") || s.equals("MsgCount") ||
        s.equals("DSeconds") || s.equals("RadiusofCurve") ||
        s.equals("Confidence") || s.equals("PathCount") ||
        s.equals("RelTime") || s.equals("DeltaTime") ||
        s.equals("Latitude") || s.equals("Longitude")
    )
    val smallerDf = df.select(cols.head, cols.tail: _*)

    // bin data
    val bucketized = utils.SparkUtils.bucketize(smallerDf, BUCKET_SPECS)
    bucketized.write.format("csv")
      .partitionBy(groupIndices: _*)
      .option("header", "true")
      .mode("overwrite")
      .save(BINNED_DATA_DIR)
    println("Done with data binning")

  }

  def main(args: Array[String]): Unit = {
    binData()

    for {(groupVal, path) <- utils.FileUtils.getDataPath(groupIndices, BINNED_DATA_DIR)} {
      val dfBinned = spark.read.format("csv")
        .option("header", "true")
        .option("inferSchema", "true")
        .load(path)
      val subpath = groupIndices.zip(groupVal).map { case (fname, value) => s"$fname=$value" }.mkString("/")
      spmd.Profiling.doProfile(features, dfBinned, HIST_DIR + "/" + subpath)
    }
  }
}

trait BasicFeatureSet {
  def readMyData(source: String): DataFrame =
    spmd.DataSetUtil.readDF(source)
}


trait ExtendedFeatureSet {
  def readMyData(source: String): DataFrame = {
    val basic = spmd.DataSetUtil.readDF(source)
    spmd.DataSetUtil.readExtraFeatures(basic)
  }
}

trait ExtendedFeatureSetWithFileHeader {
 def readMyData(source: String): DataFrame = {
    val basic = spmd.DataSetUtil.readDF(source, "true")
    spmd.DataSetUtil.readExtraFeatures(basic)
  } 
}
