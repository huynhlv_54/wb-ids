package spmd

import org.apache.spark.sql.DataFrame
import utils.SparkUtils.histogramming

object Profiling extends utils.SparkInit {
  def doProfile(inputFileOrDir: String, outputDir: String): List[DataFrame] = {
    val input = if (inputFileOrDir.endsWith(".csv")) inputFileOrDir
    else (inputFileOrDir + "/*.csv")

    val df = spark
      .read
      .format("csv")
      .option("header", "true")
      .option("inferSchema", "true")
      .load(input)

    doProfile(df, outputDir)
  }

  def doProfile(df: DataFrame, outputDir: String): List[DataFrame] =
    doProfile(FeatureDef.allFeatures, df, outputDir)

  def doProfile(features: List[List[String]], df: DataFrame, outputDir: String): List[DataFrame] = {
    val columns = df.columns.toSet
    features.filter(_.toSet.subsetOf(columns))
      .map(histogramming(_, df, outputDir))
  }

  def doProfile(features: List[List[String]], df: DataFrame): List[DataFrame] = {
    val columns = df.columns.toSet
    features.filter(_.toSet.subsetOf(columns))
      .map(histogramming(_, df))
  }

  def main(args: Array[String]): Unit = {
    if (args.length != 2) {
      println("Usage:")
      println("<spmd.ProfileAll> <Input directory with CSV files> <Output directory>")
      System.exit(0)
    }
    doProfile(args(0), args(1))
  }
}
