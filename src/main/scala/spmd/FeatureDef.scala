package spmd

object FeatureDef {
  // Left and right of a window
  val ls = List(0, 1, 2, 3)
  val rs = List(1, 1, 2, 3)
  val windows = ls.zip(rs)

  val features1D = List(
    List("RoadType"),
    //List("Latitude"),
    //List("Longitude"),
    //List("Elevation"),
    List("Speed"),
    //List("Heading"),
    List("Ax"),
    List("Ay"),
    List("Az"),
    List("YawRate")
      //List("DeltaHeading")
  )

  val features2D = List(
    //List("Latitude", "Longitude"),
    List("Speed", "Ax"),
    List("Speed", "Ay"),
    List("Speed", "Az"),
    List("Speed", "YawRate"),
    List("Ax", "Ay"),
    List("Ax", "Az"),
    List("Ax", "YawRate"),
    List("Ay", "Az"),
    List("Ay", "YawRate"),
    List("Az", "YawRate")
  )

  val features3D = List(
    // List("Latitude", "Longitude", "Elevation"),
    List("Ax", "Ay", "Az"),
    List("Speed", "Ax", "Ay"),
    List("Speed", "Ax", "YawRate"),
    List("Speed", "Ay", "YawRate"),
    List("Speed", "Az", "YawRate"),
    List("Ax", "Ay", "YawRate"),
    List("Ax", "Az", "YawRate"),
    List("Ay", "Az", "YawRate"))

  val featuresnD = List(
    List("Speed", "Ax", "Ay", "Az"),
    List("Speed", "Ax", "Ay", "YawRate"),
    List("Speed", "Ax", "Ay", "Az", "YawRate")
  )

  val extendedFeatures = List(
    //List("DeltaElevation"),
    //List("Axy"),
    //List("DeltaTime"),
    //List("Speed", "lead_Speed_1"),
    //List("Speed", "lag_Speed_1"),
    //List("Speed", "lead_Speed_1", "lag_Speed_1"),
    //List("Speed", "lead_Speed_1", "lag_Speed_1", "lead_Speed_2", "lag_Speed_2"),
    //List("Ax", "lead_Ax_1"),
    //List("Ax", "lag_Ax_1"),
    //List("Ax", "lead_Ax_1", "lag_Ax_1"),
    //List("Ax", "lead_Ax_1", "lag_Ax_1", "lead_Ax_2", "lag_Ax_2"),
    //List("Ay", "lead_Ay_1"),
    //List("Ay", "lag_Ay_1"),
    //List("Ay", "lead_Ay_1", "lag_Ay_1"),
    //List("Ay", "lead_Ay_1", "lag_Ay_1", "lead_Ay_2", "lag_Ay_2"),
    //List("YawRate", "lead_YawRate_1"),
    //List("YawRate", "lag_YawRate_1"),
    //List("YawRate", "lead_YawRate_1", "lag_YawRate_1"),
    //List("YawRate", "lead_YawRate_1", "lag_YawRate_1", "lead_YawRate_2", "lag_YawRate_2"),

    //List("Speed", "lead_Speed_1", "Ax", "Ay"),
    //List("Speed", "lead_Speed_1", "Ax", "Ay", "YawRate"),
    //List("Speed", "lead_Speed_1", "lag_Speed_1", "Ax", "Ay")
      //List("Speed", "lead_Speed_1", "Ax", "lead_Ax_1", "Ay", "lead_Ay_1"),
      //List("Speed", "lead_Speed_1", "lag_Speed_1", "Ax", "lead_Ax_1", "lag_Ax_1", "Ay", "lead_Ay_1", "lag_Ay_1"),
      // List("Speed", "lead_Speed_1", "lag_Speed_1", "lead_Speed_2", "lag_Speed_2", "Ax", "lead_Ax_1", "lag_Ax_1", "lead_Ax_2", "lag_Ax_2", "Ay", "lead_Ay_1", "lag_Ay_1", "lead_Ay_2", "lag_Ay_2"),

    //List("Speed", "lead_Speed_1", "Ay", "lead_Ay_1", "YawRate", "lead_YawRate_1"),
    // List("Speed", "lead_Speed_1", "lag_Speed_1", "Ay", "lead_Ay_1", "lag_Ay_1", "YawRate", "lead_YawRate_1", "lag_YawRate_1"),
    // List("Speed", "lead_Speed_1", "lag_Speed_1", "lead_Speed_2", "lag_Speed_2", "Ay", "lead_Ay_1", "lag_Ay_1", "lead_Ay_2", "lag_Ay_2",
    //   "YawRate", "lead_YawRate_1", "lag_YawRate_1", "lead_YawRate_2", "lag_YawRate_2"),

    //List("Speed", "lead_Speed_1", "Ax", "lead_Ax_1", "YawRate", "lead_YawRate_1"),
    // List("Speed", "lead_Speed_1", "lag_Speed_1", "Ax", "lead_Ax_1", "lag_Ax_1", "YawRate", "lead_YawRate_1", "lag_YawRate_1"),
    // List("Speed", "lead_Speed_1", "lag_Speed_1", "lead_Speed_2", "lag_Speed_2", "Ax", "lead_Ax_1", "lag_Ax_1", "lead_Ax_2", "lag_Ax_2",
    //   "YawRate", "lead_YawRate_1", "lag_YawRate_1", "lead_YawRate_2", "lag_YawRate_2"),

    //List("Speed", "lead_Speed_1", "Ax", "lead_Ax_1", "Ay", "lead_Ay_1", "YawRate", "lead_YawRate_1")
    // List("Speed", "lead_Speed_1", "lag_Speed_1", "Ax", "lead_Ax_1", "lag_Ax_1", "Ay", "lead_Ay_1", "lag_Ay_1", "YawRate", "lead_YawRate_1", "lag_YawRate_1"),
    // List("Speed", "lead_Speed_1", "lag_Speed_1", "lead_Speed_2", "lag_Speed_2", "Ax", "lead_Ax_1", "lag_Ax_1", "lead_Ax_2", "lag_Ax_2", "Ay", "lead_Ay_1", "lag_Ay_1", "lead_Ay_2", "lag_Ay_2",
    //   "YawRate", "lead_YawRate_1", "lag_YawRate_1", "lead_YawRate_2", "lag_YawRate_2")
  ) ++
  windows.flatMap{case (l: Int, r: Int) => {
    List(//List(s"Distance_${l}_${r}"),
         // List(s"Azimuth_${l}_${r}"),
         // List(s"DeducedSpeed_${l}_${r}"),
         // List(s"DeducedAxy_${l}_${r}"),
         // List(s"DeducedYawRate_${l}_${r}"),
      List(s"DiffSpeed_${l}_${r}"),
      List(s"DiffHeading_${l}_${r}"),
      // List(s"DiffAxy_${l}_${r}"),
      // List(s"DiffYawRate_${l}_${r}"),
      List("Speed", s"DeducedSpeed_${l}_${r}"),
      //List("Speed", s"DeducedSpeed_${l}_${r}", "Ax", "Ay"),
      List("Heading", s"Azimuth_${l}_${r}")
        // List("Axy", s"DeducedAxy_${l}_${r}"),  // Covered by (Speed, Ax, Ay)
        // List("YawRate", s"DeducedYawRate_${l}_${r}"),
        // List(s"DiffSpeed_${l}_${r}", "Ax", "Ay"),
        // List(s"DiffSpeed_${l}_${r}", s"DiffHeading_${l}_${r}")
    )
    //++ List(s"DiffSpeed_${l}_${r}", s"DiffHeading_${l}_${r}", s"DiffAxy_${l}_${r}", s"DiffYawRate_${l}_${r}").combinations(3).toList
  }} // ++ List(
     //   List("Speed", "DeducedSpeed_0_1", "DeducedSpeed_1_1", "Ax", "Ay"),
     //   List("Speed", "DeducedSpeed_1_1", "DeducedSpeed_2_2", "Ax", "Ay"),
     //   List("Speed", "DeducedSpeed_0_1", "DeducedSpeed_1_1", "Ay", "YawRate"),
     //   List("Speed", "DeducedSpeed_1_1", "DeducedSpeed_2_2", "Ay", "YawRate")
     // )
  /* ++
   List("Speed").map(f => List(f,
   s"Diff${f}_0_1",
   s"Diff${f}_1_1")) ++
   List("Speed").map(f => List(f,
   s"Diff${f}_1_1", 
   s"Diff${f}_2_2")) ++
   List("Speed").map(f => List(f,
   s"Diff${f}_0_1",
   s"Diff${f}_1_1", 
   s"Diff${f}_2_2") ++
   List("Speed").map(f => List(f,
   s"Diff${f}_1_1", 
   s"Diff${f}_2_2", "Ax", "Ay"))
   )*/

  val basicFeatures: List[List[String]] = features1D ++ (features2D ++ features3D) ++ featuresnD
  val allFeatures: List[List[String]] = basicFeatures ++ extendedFeatures
  val requiredColumns: Set[String] = allFeatures.flatMap(s => s).toSet
}
