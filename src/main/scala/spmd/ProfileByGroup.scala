package spmd

import java.io.File

import org.apache.commons.io.FileUtils
import org.apache.commons.io.filefilter.{DirectoryFileFilter, NotFileFilter, TrueFileFilter}
import utils.SparkUtils.histogramming

import scala.collection.JavaConverters._

class ProfilerByGroup(val groupDef: List[String]) extends utils.SparkInit {

  val groupDefSet = groupDef.toSet

  def doProfiling(inputDir: String, outputDir: String): Unit = {
    // exclude all features used in grouping
    val features = FeatureDef.allFeatures
      .filter({f: List[String] => f.toSet.intersect(groupDefSet).isEmpty }) // exclude all features used in grouping

    for {(groupVal, path) <- utils.FileUtils.getDataPath(groupDef, inputDir)} {
      val df = spark.read
        .format("csv")
        .option("header", "true")
        .option("inferSchema", "true")
        .load(path + "/*.csv")

      val sd = groupDef.zip(groupVal)
        .map{case (fname: String, fval: Int) => "%s_%d".format(fname, fval)}
        .mkString("_")
      
      val subOutputDir = outputDir + "/" + sd
      val columns = df.columns.toSet
      for (f <- features) {
        if (f.toSet.subsetOf(columns))
          histogramming(f, df, subOutputDir)
      }
    }
  }
}

object ProfilingByGroup {
  def main(args: Array[String]): Unit = {
    if (args.length <= 2) {
      println("Usage:")
      println("<spmd.ProfileBSMByLoc> <inputDir> <outputDir> <Features used for grouping>")
      System.exit(1)
    }
    val groupDef = args.toList.drop(2)
      (new ProfilerByGroup(groupDef)).doProfiling(args(0), args(1))
  }
}
