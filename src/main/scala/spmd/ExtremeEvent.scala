package spmd


// Spark for heavy weight DataFrame
import org.apache.spark.sql.{DataFrame, Row}
import org.apache.spark.sql.functions._
import spmd.DataSetUtil._
import utils.BucketDef._


object ExtremeEvent extends utils.SparkInit {
  import spark.implicits._
  /**
    * Given a list of threshold (t0, t1, ..., tn)
    * return a function that computes the class of a value t
    * @param thresholds
    * @return
    *         t < t0 => 0
    *         t < t1 => 1
    *         ...
    */
  def digitize[T](thresholds: Array[T])(v: T)(implicit num: Numeric[T]): Int = {
    var myClass: Int = 0
    while((myClass < thresholds.length) && num.lt(thresholds(myClass), v))
      myClass += 1
    myClass
  }

  /*
   * Bucketize a value
   * @param splits = [s0, s1, ..., sn]
   * @return
   *         t < s0 => -1
   *         t < s1 => 0
   *         ...
   */
  def bucketize[T](splits: Array[T])(v: T)(implicit num: Numeric[T]): Int = {
    digitize(splits)(v)(num) - 1
  }

  /**
    * Given a profile P (A dataframe with col f1, f2, ..., fn, freq)
    * An index column fc and another column fv
    * Return adaptive threshold of fv according to fc  
    */
  def getAdaptiveThreshold(df: DataFrame, bucketDef: String,
    indexFeature: String, valFeature: String, quantiles: List[Double]):
      (Double, Double) => Int = {
    val bucketizer = readBucketSpecs(bucketDef, Array(indexFeature))
    val splits = bucketizer.getSplitsArray(0)
    val indices = (0 until splits.length).toList

    val dfSafeRange = df.filter((col(indexFeature) >= splits(0)) && (col(indexFeature) <= splits.last))
    val dfWithIndex = bucketizer.transform(dfSafeRange)

    dfWithIndex.createTempView("bsm")
    val quantilesQuery = quantiles
      .map({q: Double => "PERCENTILE_APPROX(%s, %f) AS `quantile-%f`".format(valFeature, q, q)})
      .mkString(" ,")
    val query = "SELECT `%s-binned`, %s FROM bsm GROUP BY `%s-binned`".format(indexFeature, quantilesQuery, indexFeature)

    val df2 = spark.sql(query)
    val adaptiveThreshold = df2.rdd.collect.map({r: Row => {
      (
        r.getAs[Int]("%s-binned".format(indexFeature)),
        quantiles.map({q: Double => r.getAs[Double]("quantile-%f".format(q))}).toArray
      )
    }}).toMap

    val classifier = {(i: Double, v: Double) => {
      val iInt = bucketize(splits)(i)
      val thresholds = adaptiveThreshold.getOrElse(iInt, Array[Double]())
      digitize(thresholds)(v)
    }}
    classifier
  }


  /**
    * Compute adaptive threshold for profiled and binned data
    * @param profileFile Pre-calculated profile
    * @param indexFeature
    * @param valFeature
    * @param quantiles
    * @return
    */
  def getAdaptiveThresholdForBinnedData(profileFile: String,
    indexFeature: String, valFeature: String,
    quantiles: List[Double]): (Int, Int) => Int = {

    println(profileFile)
    val df = spark.read
      .format("csv")
      .option("inferSchema", "true")
      .option("header", "true")
      .load(profileFile)
      .na.drop()

    val indices = df.select(indexFeature).distinct.as[Int].rdd.collect
    val adaptiveThreshold = {for {
      i <- indices
      subDf = df.filter(col(indexFeature) === i)
      thresholds = utils.SparkStats.computeQuantiles(subDf, valFeature, quantiles).toArray
    } yield (i, thresholds)}.toMap

    val classifier = {(i: Int, v: Int) => {
      val t = adaptiveThreshold.getOrElse(i, Array[Int]())
      digitize[Int](t)(v)
    }}
    classifier
  }


  /**
    * Classify un-binned data
    * @param df     DataFrame of BSM
    * @param bucketDef   json file contain the info how to bucketize features  (needed to bucketize the index feature)
    * @return                   df, with column "Speed-Axy-Class" 
    */
  def eventFromOrigData(df: DataFrame, bucketDef: String): DataFrame = {
    val axyCol = udf(magnitude).apply(col("Ax"), col("Ay"))
    val dfWithAxy = df.withColumn("Axy", axyCol)
    dfWithAxy.persist()

    val classifier = getAdaptiveThreshold(dfWithAxy, bucketDef, "Speed", "Axy", List(0.95))
    val classes = udf(classifier).apply(col("Speed"), col("Axy"))

    dfWithAxy.withColumn("Speed-Axy-Class", classes)
      .drop("Axy")
  }

  /**
    * Classify binned data to normal - extreme event
    * @param df                 DataFrame of binned messages
    * @param speedAxyProfile    Location of Speed-Axy histogram
    * @return                   df, with column "Speed-Axy-Class" 
    */
  def eventsFromApproxQuantiles(df: DataFrame, speedAxyProfile: String): DataFrame = {
    val classifier = ExtremeEvent.getAdaptiveThresholdForBinnedData(speedAxyProfile, "Speed", "Axy", List(0.95))
    val classes = udf(classifier).apply(col("Speed"), col("Axy"))
    df.withColumn("Speed-Axy-Class", classes)
  }

  def doClassify(df: DataFrame, bucketDef: String, outputDir: String): DataFrame = {
    val res = eventFromOrigData(df, bucketDef).select("Speed-Axy-Class", df.columns: _*)

    res.write
      .partitionBy("Speed-Axy-Class")
      .format("csv")
      .option("header", "true")
      .mode("overwrite")
      .save(outputDir)

    res
  }

  def main(args: Array[String]): Unit = {
    if (args.length != 3) {
      println("Usage:")
      println("spmd.ExtremeEvent <BSM file> <BucketDef> <Output Dir>")
      System.exit(0)
    }
    val (bsmFile, bucketDef, outputDir) = (args(0), args(1), args(2))
    doClassify(spmd.DataSetUtil.readDF(bsmFile), bucketDef, outputDir)
  }
}


object QuickExtremeEvent extends utils.SparkInit {
  def main(args: Array[String]){
    if (args.length != 3) {
      println("Usage:")
      println("spmd.ExtremeEvent <Bucketized BSM file> <Speed-Axy profile> <Output Dir>")
      System.exit(0)
    }

    val (bsmFile, speedAxyProfile, outputDir) = (args(0), args(1), args(2))
    val df = spark.read
      .format("csv")
      .option("inferSchema", "true")
      .option("header", "true")
      .load(bsmFile)
    ExtremeEvent.eventsFromApproxQuantiles(df, speedAxyProfile).write
      .partitionBy("Speed-Axy-Class")
      .format("csv")
      .option("header", "true")
      .mode("overwrite")
      .save(outputDir)
  }
}
