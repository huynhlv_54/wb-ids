package spmd


import org.apache.spark.rdd.RDD
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.{ DataFrame, Row }
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions.{col, lag, sum, udf}



/**
  * Extract message sequences from a collection of BSM. 
  *  A message sequence is defined by:
  * + TxDevice
  * + TxRandom
  * + Gentime of two consecutive messages differ at most TimeSplitThreshold
  */
object Sequence extends utils.SparkInit{
  val TimeSplitThreshold = 1000000.0  // 1e6 nanosecs = 1 sec
  val sequenceIndex = List("RxDevice","TxDevice", "TxRandom")
  import spark.implicits._

  /**
    * Extract a column ("Session")  such that ("Session", "RxDevice", "TxRandom") uniquely 
    * identify a continuous message stream (no missing messages)
    * This method expects that Gentime is in the original format
    */
  def toSequenceDF(df: DataFrame): DataFrame = {
    // Identify messages from the same device
    val wDevice = Window.partitionBy(sequenceIndex.head, sequenceIndex.tail: _*).orderBy("Gentime")
    val wCumSum = Window.partitionBy(sequenceIndex.head, sequenceIndex.tail: _*).orderBy("Gentime")
      .rowsBetween(Long.MinValue, 0)

    val sessionDiff: (Double, Double, Int, Int) => Long =
      (time, prevTime, msgCount, prevMsgCount) => if (
        (time - prevTime < TimeSplitThreshold) && (msgCount + 128 - prevMsgCount) % 128 == 1)
        0 else 1
    val sessionInc = udf(sessionDiff).apply(
      col("Gentime"), lag("Gentime", 1).over(wDevice),
      col("MsgCount"), lag("MsgCount", 1).over(wDevice))

    df.withColumn("SessionInc", sessionInc)
      .na.fill(1, Seq("SessionInc"))
      .withColumn("Session", sum("SessionInc").over(wCumSum))
      .select("Session", df.columns: _*)
  }



  /**
    * Given a dataframe of BSM, with additional "Session" field, 
    * Splits them 
    */
  def separateSequences(df: DataFrame): RDD[Array[Row]] = {
    // Original message format
    val columns: Array[String] = df.columns

    df.rdd.groupBy(r => (r.getAs[Long]("Session"), r.getAs[Int]("TxDevice"), r.getAs[Int]("TxRandom")))    // RDD[(k, Iter[Row])]
      .values          // RDD[Iter[Row]]
      .map(_.toArray)  // RDD[Array[Row]]
  }

  /**
    * Store all BSM sequences in a data frame
    */
  def sequencesToDF(seqs: Array[BsmSeq]): DataFrame = {
    // Set different pseudonym for different sequnces, even if the
    // sequences originally came from the same device
    // This is to make sure different attacks don't mixed together'
    val seqsNewIndices = seqs.zipWithIndex
      .map{ case (seq: BsmSeq, index: Int) => seq.map(bsm => bsm.copy(TxDevice = index))}
    seqsNewIndices.flatten.toSeq.toDF()
  }


  private val rng = new scala.util.Random(1234) // fixed seed: 1234 for more predictability
  /**
    * Change a message at a given index
    */
  private def changeMessage(sequence: BsmSeq, msgIndex: Int, changer: BSMP1 => BSMP1Mod): BsmModSeq = {
    val newMsg = changer(sequence(msgIndex))
    val sequenceWithChangedField = sequence.map(msg => new BSMP1Mod(msg))
    sequenceWithChangedField.patch(msgIndex, Seq(newMsg), 1)
  }


  /**
    * Select a random message in a sequence that satisfy a condition and change it
    * 
    * @param cond The condition when the change can be applied
    * @param changer the method to generate a new BSMP1 from a BSMP1
    * @return (changed: Boolean, msgSequence: newMsgSequence) changed = true if a message
    * has been changed. 
    */
  def changeRandomMessage(sequence: BsmSeq, cond: BSMP1 => Boolean, changer: BSMP1 => BSMP1Mod): Option[BsmModSeq] = {
    val t = sequence.zipWithIndex
      .filter{case (msg: BSMP1, i: Int) => cond(msg)} // choose changeable messages
      .map{case (msg: BSMP1, i: Int) => i}            // ge the index

    if (t.isEmpty) None
    else {
      val index = t(rng.nextInt(t.length))
      Some(changeMessage(sequence, index, changer))
    }
  }

  
  // Convert a row to BSM
  // Require that r has the correct schema
  def rowToBsm(r: Row): BSMP1 = {
    BSMP1(
      r.getAs("RxDevice"),
      r.getAs("FileID"),
      r.getAs("TxDevice"),
      r.getAs("Gentime"),
      r.getAs("TxRandom"),
      r.getAs("MsgCount"),
      r.getAs("DSeconds"),
      r.getAs("Latitude"),
      r.getAs("Longitude"),
      r.getAs("Elevation"),
      r.getAs("Speed"),
      r.getAs("Heading"),
      r.getAs("Ax"),
      r.getAs("Ay"),
      r.getAs("Az"),
      r.getAs("YawRate"))
      // r.getAs("PathCount"),
      // r.getAs("RadiusofCurve"),
      // r.getAs("Confidence"))
  }
  

  /**
    * From a window, create one attack from each changeable message
    * 
    * @param cond The condition when the change can be applied
    * @param changer the method to generate a new BSMP1 from a BSMP1
    * @return Option[BSMModSeq]
    */
  def changeMessage(sequence: BsmSeq, cond: BSMP1 => Boolean, changer: BSMP1 => BSMP1Mod): Option[BsmModSeq] = {
    val margin = (sequence.length - 1) / 2
    val msg = sequence(margin)
    if (cond(msg)) {
      Some(changeMessage(sequence, margin, changer))
    } else None
  }
}
