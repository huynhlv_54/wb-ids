package spmd

import org.apache.spark.sql.{DataFrame, Row}

object PartitionByFeatures extends utils.SparkInit {
    /**
      * Partition data by a list of specified features
      * Intended to work with digitized data
      */
    def partitionByFeatures(features: List[String])(inputDir: String, outputDir: String): Unit = {
        val df = spark
            .read
            .format("csv")
            .option("header", "true")
            .option("inferSchema", "true")
            .load(inputDir + "/*.csv")
        df.write
            .partitionBy(features:_*)
            .format("csv")
            .option("header", "true")
            .mode("overwrite")
            .save(outputDir)
    }

    // /** TODO */
    // def partitionByFeatures(features: List[String])(df: DataFrame): List[(String, DataFrame)] = {
    //     df.createTempView("mytmptable")
    //     // extract all distinct values of the given features
    //     var indices = Map[(String, List[Int])]
    //     for (f <- features) {
    //         val values = df.select(f).distinct.collect.map({r: Row => r.getAs[Int]}(f)).toList
    //         indices = indices + (f, values)
    //     }

    //     // Select data 
    //     null
    // }

    def main(args: Array[String]): Unit = {
        if (args.length <= 2) {
            println("Usage: ")
            println("{spmd.PartitionByFeatures} <inputDir> <outputDir> <List of feature to partition by>")
            System.exit(1)
        }
        val inputDir = args(0)
        val outputDir = args(1)
        val features = args.toList.drop(2)

        partitionByFeatures(features)(inputDir, outputDir)
    }
}
