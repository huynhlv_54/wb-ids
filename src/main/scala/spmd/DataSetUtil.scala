package spmd

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions.{col, first, lead, lag, udf, avg, when, lit, abs}
import org.apache.spark.sql.types.{ DoubleType, IntegerType, StructField, StructType }

object DatasetConstants {
  // unit of one
  def TIME_SCALE = 1000000.0 // nanosec

  // get the timestamp in seconds
  def getTimestamp: (Long => Double) = {t => t / 1000000.0 - 35.0 + 1072933200.0}
}

object DataSetUtil extends utils.SparkInit {
  def magnitude: (Double, Double) => Double = {(x, y) =>
    math.sqrt(x * x + y * y)
  }

  val windows = FeatureDef.windows

  def headers(): Array[String] = {
    val headerCSV = spark
      .read
      .format("csv")
      .option("header", "true")
      .load("data/spmd/bsm/bsm_head_trimmed.csv")
    headerCSV.columns
  }

  /**
    * Read a fraction of BSMPart1 dataset
    * Convert timestamp
    * Compute Axy
    * Compute distance to the next point
    */
  def readExtraFeatures(inputFileOrDir: String): DataFrame = {
    val df = readDF(inputFileOrDir)
    readExtraFeatures(df)
  }


  val diffAngle = (a1: Double, a2: Double) => {
    var d = a1 - a2
    while (d > 180.0) d = d - 360.0
    while (d < -180) d = d + 360.0
    d
  }

  def readExtraFeatures(_df: DataFrame): DataFrame = {
    val df = Sequence.toSequenceDF(_df)
    df.createOrReplaceTempView("orig_tbl")

    val wString = "stream AS (PARTITION BY RxDevice, TxDevice, TxRandom, Session ORDER BY Gentime)"

    val llFeatures = List("Gentime", "Latitude", "Longitude", "Elevation", "Speed", "Heading", "Ax", "Ay", "YawRate")
    val lags = windows.map{case (l, _) => l}
      .distinct
      .map(l => llFeatures.map(f => s"lag($f, $l) OVER stream AS lag_${f}_$l")
        .mkString(",\n"))
      .mkString(",\n")
    val leads = windows.map{case (_, r) => r}
      .distinct
      .map(r => llFeatures.map(f => s"lead($f, $r) OVER stream AS lead_${f}_$r")
        .mkString(",\n"))
      .mkString(",\n")

    val query = s"""
SELECT *,
Gentime - (FIRST(Gentime) OVER stream) AS RelTime, 
$lags,
$leads
FROM orig_tbl
WINDOW $wString"""
    var res = spark.sql(query)

    // Distance and azimuth
    val daSchemas = windows.map{case (l: Int, r: Int) => ((l + r),
      StructType(Array(
        StructField(s"Distance_${l}_${r}", DoubleType, true),
        StructField(s"Azimuth_${l}_${r}", DoubleType, true))))}
      .toMap

    val deduceds = windows.map{case (l, r) => ((l, r),
      Map(
        ("DeltaTime", (col(s"lead_Gentime_$r") - col(s"lag_Gentime_$l")).divide(DatasetConstants.TIME_SCALE)),
        ("DistanceAndAzimuth", udf(utils.GeoCalc.getDistanceAndAzimuth, daSchemas(l + r))
          .apply(
            col(s"lag_Latitude_$l"), col(s"lag_Longitude_$l"),
            col(s"lead_Latitude_$r"), col(s"lead_Longitude_$r"))),
        ("Speed", col(s"Distance_${l}_${r}") / col(s"DeltaTime_${l}_${r}")),
        ("Axy", abs((col(s"lead_Speed_$r") - col(s"lag_Speed_$l"))) /
          col(s"DeltaTime_${l}_${r}")),
        ("YawRate", udf(diffAngle).apply(col(s"lead_Heading_$r"), col(s"lag_Heading_$l")) / col(s"DeltaTime_${l}_${r}"))
      ))}
      .toMap
    
    for ((l, r) <- windows) {
      val columns = res.columns
      res = res.withColumn(s"DA_${l}_${r}", deduceds((l, r))("DistanceAndAzimuth"))
        .select(columns.head, (columns.tail ++ Array(
          s"DA_${l}_${r}.Distance_${l}_${r}",
          s"DA_${l}_${r}.Azimuth_${l}_${r}")): _*)
        .drop(s"DA_${l}_${r}")

      res = res.withColumn(s"DeltaTime_${l}_${r}", deduceds((l, r))("DeltaTime"))
        .withColumn(s"DeducedSpeed_${l}_${r}", deduceds((l, r))("Speed"))
        .withColumn(s"DeducedYawRate_${l}_${r}", deduceds((l, r))("YawRate"))
    }

    val keep = _df.columns.toSet union FeatureDef.requiredColumns
    val columns = res.columns.filter(keep.contains(_))
    res.select(columns.head, columns.tail: _*)
  }


  /**
    * Read a fraction of BSMPart1 dataset
    * Apply no conversion
    */
  def readDF(inputFileOrDir: String, header: String = "false"): DataFrame = {
    val input = if ((new java.io.File(inputFileOrDir)).isDirectory()) (inputFileOrDir + "/*.csv")
    else inputFileOrDir

    val df = spark.read
      .format("csv")
      .option("inferSchema", "true")
      .option("header", header)
      .load(input)
    if (header.equals("false")) df.toDF(headers(): _*)
    else df
  }
}
