package spmd

case class BSMP1 (
  RxDevice: Int,
  FileID: Int,
  TxDevice: Int,
  Gentime: Long,
  TxRandom: Int,
  MsgCount: Int,
  DSeconds: Int,
  Latitude: Double,
  Longitude: Double,
  Elevation: Double,
  Speed: Double,
  Heading: Double,
  Ax: Double,
  Ay: Double,
  Az: Double,
  YawRate: Double
    // PathCount: Int,
    // RadiusofCurve: Double,
    // Confidence: Int
)

case class BSMP1Mod (
  bsmp1: BSMP1,
  changed: Int) {

  def this(bsm: BSMP1) = this(bsm, 0)
}
