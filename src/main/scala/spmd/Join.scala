package spmd

import org.apache.spark.sql.DataFrame

/**
  * Join all parts of BSM part 1
  * 
  *  Note: Many of the msg have these optional parts "unavailable"
  */
object JoinData {

    /* Note: After testing, we may combine all the join methods below
     */

    def joinEvents (bsmp1: DataFrame, events: DataFrame): DataFrame = {
        bsmp1.join(events, Seq("RxDevice", "TxDevice", "Gentime", "FileId"), "left_outer")
    }

    /**
      * TODO: Check if Brake events are part of BSM partI
      */
    def joinBrakeBytes (bsmp1: DataFrame, bb1: DataFrame, bb2: DataFrame): DataFrame = {
        val df = bsmp1.join(bb1,
            bsmp1.col("RxDevice") === bb1.col("RxDevice")
                && bsmp1.col("TxDevice") === bb1.col("TxDevice")
                && bsmp1.col("FileId") === bb1.col("FileId")
                && bsmp1.col("Gentime") >= bb1.col("StartTime")
                && bsmp1.col("Gentime") <= bb1.col("EndTime"), "left_outer")
            .withColumnRenamed("Value", "BB1Value")
            .join(bb2,
                bb2.col("RxDevice")
                && bsmp1.col("TxDevice") === bb2.col("TxDevice")
                && bsmp1.col("FileId") === bb2.col("FileId")
                && bsmp1.col("Gentime") >= bb2.col("StartTime")
                    && bsmp1.col("Gentime") <= bb2.col("EndTime"), "left_outer")
        df
    }

    def joinBrakeByte2 (bsmp1: DataFrame, bb2: DataFrame): DataFrame = {

        ???
    }



}
