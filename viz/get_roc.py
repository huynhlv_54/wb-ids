#!/usr/bin/env python3

import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D


def e_distance(x, y, x1, y1):
    return max(abs(x - x1), abs(y - y1))


df1 = pd.read_csv("FPR.csv")
df2 = pd.read_csv("TPR.csv")

# Plot
fprs = [df1["D{}".format(i)] for i in range(1, 10)]
tprs = [df2["D{}".format(i)] for i in range(1, 10)]
labels = ["D{}".format(i) for i in range(1, 10)]

figure = plt.figure("ROC", figsize=(12, 12))
for i in range(len(labels)):
    fpr = fprs[i] + [1.0]
    tpr = tprs[i] + [1.0]

    # make sure tpr and fpr have the same length
    length = min(len(fpr), len(tpr))
    fpr = fpr[:length] 
    tpr = tpr[:length]

    # remove too close points
    diff_tpr = [1] + [e_distance(*p) for p in zip(fpr, tpr, fpr[1:], tpr[1:])]
    z = list(zip(fpr, tpr, diff_tpr))
    filtered_z = [(f, t) for (f, t, d) in z if d > 0.01]
    fpr = [z[0] for z in filtered_z]
    tpr = [z[1] for z in filtered_z]

    # plot
    if (i + 1 != 3):
        # linestyle = ":"
        plt.plot(fpr, tpr, label=labels[i], marker=Line2D.filled_markers[i], linestyle=":")

plt.xlabel("False Positive Rate", fontsize=20)
plt.ylabel("True Positive Rate", fontsize=20)
plt.legend(loc=0)
plt.grid(True)

figure.savefig("ROC.eps", transparent=True)
plt.close(figure)
