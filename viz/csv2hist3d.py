#!/usr/bin/env python3

import pandas as pd
import numpy as np

from mayavi import mlab

import json
import sys


def draw_scatter3d(df, features):
    name = "-".join(features)

    xx = df[features[0]].values
    yy = df[features[1]].values
    zz = df[features[2]].values
    freq = df["freq"].values

    figure = mlab.figure(name)
    pts = mlab.points3d(xx, yy, zz, freq, scale_mode="none")
    # pts = mlab.points3d(xx, yy, zz, freq, scale_mode="none", opacity=0.5, scale_factor=0.1)
    # pts = mlab.points3d(xx, yy, zz, freq, scale_mode="none", mode="cube", opacity=0.5, scale_factor=1)
    mlab.axes()
    mlab.show()

    # Create a hollowed figure
    mx = np.median(xx)
    my = np.median(yy)
    mz = np.median(zz)
    df2 = df[(df[features[0]] >= mx) | (
        df[features[1]] >= my) | (df[features[2]] <= mz)]
    print("Cut off by: ", mx, my, mz)
    xx = df2[features[0]].values
    yy = df2[features[1]].values
    zz = df2[features[2]].values
    freq2 = df2["freq"].values

    figure2 = mlab.figure(name + " -- hollowed")
    pts2 = mlab.points3d(xx, yy, zz, freq2, scale_mode="none")
    # pts2 = mlab.points3d(xx, yy, zz, freq2, scale_mode="none", opacity=0.5, scale_factor=0.1)
    mlab.axes()
    mlab.show()


def draw_3dhistogram(csvfile, bucket_specs):
    df = pd.read_csv(csvfile)
    features = [x for x in df.columns][:-1]
    name = "-".join(features)
    print(name)

    if len(features) == 3:
        for f in features:
            splits = bucket_specs[f]
            mids = [(x[0] + x[1]) / 2.0 for x in zip(splits, splits[1:])]
            df[f] = df[f].map(lambda bucket: mids[bucket])
        draw_scatter3d(df, features)
    else:
        print("Not implemented")


if __name__ == "__main__":
    csvfile = ""
    if len(sys.argv) == 3:
        csvfile = sys.argv[1]
        bucketfile = sys.argv[2]
    else:
        print(
            "Usage: {} [csv file containing for a 3D histogram]  [Specification of buckets]")
        sys.exit()

    # Read the bucket specifications
    bucket_specs = json.loads(open(sys.argv[2], "r").read())
    draw_3dhistogram(csvfile, bucket_specs)
