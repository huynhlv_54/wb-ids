#!/usr/bin/env python3

import pandas as pd
import matplotlib.lines as mlines
import matplotlib.pyplot as plt

subdued_yellow = "#F49E39"
ytick_unit = 1000000 # million

df = pd.read_csv("/home/vle1/workspace/wb-ids/daynight_x80.csv")
fig, ax = plt.subplots()

time_shift = 10
df["Hour"] = (df["Hour"] - time_shift) % 24
df = df.sort_values("Hour")
bar_list = plt.bar(df["Hour"], df["count(1)"] / ytick_unit)
xticks = ["{}-{}".format(i, i + 1) for i in df["Hour"]]
plt.xticks(df["Hour"], xticks, rotation=45, fontsize=10)
plt.yticks(fontsize=10)
plt.xlabel("Hour", fontsize=12)
plt.ylabel("Message frequency (million)", fontsize=12)

if time_shift == 10:
    for i in range(6, 19):
        bar_list[i].set_color(subdued_yellow)
elif time_shift == 0:
    for i in range(24):
        if i < 5 or i > 15:
            bar_list[i].set_color('r')

red_square = mlines.Line2D([], [], color=subdued_yellow, marker='s', linestyle='None',
                           markersize=10, label='Day')
blue_square = mlines.Line2D([], [], color='#1f77b4', marker='s', linestyle='None',
                            markersize=10, label='Night')
plt.legend(handles=[red_square, blue_square], fontsize=12)
plt.tight_layout()
plt.savefig("day-night.eps")
# plt.show()
