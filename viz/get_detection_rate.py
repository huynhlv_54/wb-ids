#!/usr/bin/env python3

import pandas as pd
import sys


def get_detection_rate(i):
    file_name = "{}-summary.csv".format(i)
    df = pd.read_csv(file_name)
    return df


def summarize(nthresholds):
    dfs = [get_detection_rate(i) for i in range(nthresholds)]
    df = pd.concat(dfs)
    df.to_csv("DetectionCount.csv", index=False)


if __name__ == "__main__":
    if (len(sys.argv) != 2):
        print("Usage: {} <n_thresholds>".format(sys.argv[0]))
        sys.exit(1)

    summarize(int(sys.argv[1]))
