#!/usr/bin/env python3

import pandas as pd

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import numpy as np

import json
import sys


def draw_scatter3d(df, features, midSpec):
    name = "-".join(features)
    for i in range(3):
        df[features[i]] = df[features[i]].map(midSpec[features[i]])
    
    xx = df[features[0]].values
    yy = df[features[1]].values
    zz = df[features[2]].values
    freq = df["freq"].values

    fig = plt.figure()
    # ax = fig.add_subplot(121, projection='3d')
    # ax.scatter(xx, yy, zz, c=freq,
    #            cmap="viridis",
    #            norm=colors.LogNorm(vmin=max(freq.min(), 0.1), vmax=max(freq.max(), 0.2))
    # )
    
    
    # Create a hollowed figure
    dfm = df.median(axis=0)
    mx = dfm[features[0]]
    my = dfm[features[1]]
    mz = dfm[features[2]]
    df2 = df[(df[features[0]] <= mx) | (df[features[1]] >= my) | (df[features[2]] <= mz)]
    print("Cut off by: ", mx, my, mz)
    xx = df2[features[0]].values
    yy = df2[features[1]].values
    zz = df2[features[2]].values
    freq2 = df2["freq"].values

    # 2 = fig.add_subplot(221, projection='3d')
    ax2 = fig.add_subplot(111, projection='3d')
    ax2.scatter(xx, yy, zz, c=freq2,
               cmap="viridis",
               norm=colors.LogNorm(vmin=max(freq.min(), 0.1), vmax=max(freq.max(), 0.2))
    )
    plt.show()

def draw_3dhistogram(csvfile, midSpec):
    df = pd.read_csv(csvfile)
    features = [x for x in df.columns][:-1]
    name = "-".join(features)

    if len(features) == 3:
        draw_scatter3d(df, features, midSpec)
    else:
        print("Not implemented")


if __name__ == "__main__":
    csvfile = ""
    if len(sys.argv) == 3:
        csvfile = sys.argv[1]
        bucketfile = sys.argv[2]
    else:
        print("Usage: {} [csv file containing for a 3D histogram]  [Specification of buckets]")
        sys.exit()
    
    # Read the bucket specifications
    jobjects = json.loads(open(bucketfile, "r").read())
    midSpec = dict()
    for jobj in jobjects:
        edges = jobj["bucket"]

        mid = [(a + b) / 2.0 for (a, b) in zip(edges, edges[1:])]
        midmap = dict(zip(range(len(mid)), mid))
        midSpec[jobj["feature"]] = midmap

    draw_3dhistogram(csvfile, midSpec)

        
