#!/usr/bin/env python3

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors

import sys

def draw_ROC(summary_file, output_file):
    df = pd.read_csv(summary_file)
    tpr = df["TPR"]
    fpr = df["FPR"]

    plt.title("ROC")
    plt.plot(tpr, fpr)

    plt.xlabel("True Positive Rate")
    plt.ylabel("False Positive Rate")
    
    plt.savefig(output_file)


if __name__ == "__main__":
    if (len(sys.argv) != 3):
        print("Usage:")
        print("{} <input file> <output file>".format(sys.argv[0]))
        sys.exit(1)

    draw_ROC(sys.argv[1], sys.argv[2])
    

