#!/usr/bin/env python3
import os
import sys
import fnmatch
import json

import pandas as pd
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from matplotlib.colors import ListedColormap

import seaborn as sns

FREQ_RANGE = [1, 10 ** 9]


def getUnit(data_element):
    d = data_element
    if (d.find("Speed") != -1):
        return "$m/s$"
    if (d.find("Ax") != -1 or d.find("Ay") != -1):
        return "$m/s^2$"
    if (d.find("YawRate") != -1):
        return "$degree/s$"
    if (d.find("Heading") != -1 or d.find("Azimuth") != -1):
        return "$degree$"

    return ""  # unknown unit


def getName(d):
    if (d == "Ax"):
        return "Longitudinal Acceleration"
    elif (d == "Ay"):
        return "Lateral Acceleration"
    elif (d == "YawRate"):
        return "Yaw Rate"
    elif (d.startswith("DeducedSpeed")):
        return "Estimated Speed"
    elif (d.startswith("Azimuth")):
        return "Estimated Heading"
    else:
        name_parts = d.split("_")
        if len(name_parts) == 3 and name_parts[0] == "lead" and name_parts[2] == "1":
            return "Next " + getName(name_parts[1])
        return d


def find_files(root_dir, pattern="*"):
    flist = os.listdir(root_dir)
    result = [f for f in flist if fnmatch.fnmatch(f, pattern)]
    return result


def find_files_recursive(root_dir, pattern="*"):
    results = []
    for dirpath, dirnames, filenames in os.walk(root_dir):
        for fname in filenames:
            if (fnmatch.fnmatch(fname, pattern)):
                results.append(os.path.join(dirpath, fname))

    return results


class BucketSpec:
    def __init__(self, start, end, bins):
        self.start = start
        self.end = end
        self.bins = bins
        self.step = (end - start) / bins

    def middleOfBin(self, n):
        if (n <= 0):
            return float("-inf")
        elif (n > self.bins):
            return float("inf")
        else:
            return self.start + (n - 0.5) * self.step

    def binEdges(self):
        return np.linspace(self.start, self.end, self.bins + 1)


def readBucketSpecs(jsonfile):
    with open(jsonfile) as jf:
        specs = json.loads(jf.read())

        res = dict()
        for field in specs:
            start, end, nbins = specs[field]
            res[field] = BucketSpec(start, end, nbins)

        return res


def draw_barchart(df, feature, bucketSpec, path, ylabel=""):
    bs = bucketSpec[feature]

    # Larger font size
    mpl.rc('xtick', labelsize=20)
    mpl.rc('ytick', labelsize=20)

    # x: the value of the given features
    bin_indices = np.arange(bs.bins + 2)
    x = [bs.middleOfBin(i) for i in bin_indices]

    # get the histogram
    xx = df[feature].values
    freqs = df["freq"].values
    # currently, (xx, freq) is a sparse list of (value, frequency)
    # Now fill the (value, 0) to get a dense list.
    y = np.zeros(bucketSpec[feature].bins + 2)
    for (value, freq) in zip(xx, freqs):
        y[value] = freq

    # Select appropriate range for some data
    xy = zip(x, y)
    if (feature == "Ax" or feature == "Ay"):
        xy = [p for p in xy if (p[0] >= -15 and p[0] <= 15)]
        x = [p[0] for p in xy]
        y = [p[1] for p in xy]

    figure = plt.figure(feature, figsize=(8, 8))
    plt.bar(x, y, width=bs.step, edgecolor="black")
    plt.xlabel(getUnit(feature),  fontsize=20)
    plt.ylabel("Frequency", fontsize=20)
    plt.yscale("log")
    plt.ylim(FREQ_RANGE)

    plt.tight_layout()
    figure.savefig(os.path.join(path, feature + ".eps"), transparent=True)
    figure.savefig(os.path.join(path, feature + ".png"), transparent=True)
    plt.close(figure)


def draw_colormap(df, features, bucketSpec, path):
    xlabel = features[0]
    ylabel = features[1]
    name = "-".join(features)
    figure = plt.figure(name, figsize=(8, 8))

    xbs = bucketSpec[xlabel]
    ybs = bucketSpec[ylabel]

    x_edges = xbs.binEdges()
    y_edges = ybs.binEdges()

    xx, yy = np.meshgrid(x_edges, y_edges)
    xx, yy = xx.T, yy.T
    freq = np.zeros((len(x_edges) - 1, len(y_edges) - 1))

    for index, row in df.iterrows():
        xfeature = row[xlabel]
        yfeature = row[ylabel]
        f = row["freq"]
        if ((xfeature > 0) and (xfeature <= xbs.bins) and
                (yfeature > 0) and (yfeature <= ybs.bins)):
            freq[xfeature - 1][yfeature - 1] = f

    mpl.rc('xtick', labelsize=20)
    mpl.rc('ytick', labelsize=20)
    figure = plt.figure(name, figsize=(8, 8))

    # Set the color pallete
    norm = colors.LogNorm(vmin=FREQ_RANGE[0], vmax=FREQ_RANGE[1])
    # cmap = "YlGnBu"
    # cmap = "hot"
    # cmap = sns.cubehelix_palette(8, as_cmap=True)
    cmap = ListedColormap(sns.color_palette("Blues"), 8)  # "BuGn_r", "GnBu_d"
    # cmap = sns.cubehelix_palette(light=1, as_cmap=True)
    plt.pcolormesh(xx, yy, freq, norm=norm, cmap=cmap)

    if (xlabel.find("Ax") != -1 or xlabel.find("Ay") != -1):
        plt.xlim(-15, 15)
    if (ylabel.find("Ax") != -1 or ylabel.find("Ay") != -1):
        plt.ylim(-15, 15)

    plt.colorbar()
    plt.xlabel("{} ({})".format(getName(xlabel), getUnit(xlabel)), fontsize=20)
    plt.ylabel("{} ({})".format(getName(ylabel), getUnit(ylabel)), fontsize=20)
    plt.tight_layout()

    figure.savefig(os.path.join(path, name + ".eps"), transparent=True)
    figure.savefig(os.path.join(path, name + ".png"), transparent=True)
    plt.close(figure)


def draw_histogram(csvfile, bucketSpec):
    fname = csvfile.strip().split("/")[-1]
    nfeatures = len(fname.split("-"))
    if (nfeatures > 2):
        print("Not implemented: Visualization for {}D data".format(nfeatures))
        return

    path = os.path.dirname(csvfile)
    path = os.path.join(path, "eps")
    if not os.path.exists(path):
        os.makedirs(path)
    if path == "":
        path = "./"

    # Drop rows with empty features
    df = pd.read_csv(csvfile).dropna(how='any').astype('int64')

    # Check if all features have been bucketized
    features = [x for x in df.columns][:-1]
    if not all([(x in bucketSpec) for x in features]):
        return

    name = "-".join(features)
    print(name)

    if len(features) == 1:
        draw_barchart(df, name, bucketSpec, path)
    elif len(features) == 2:
        draw_colormap(df, features, bucketSpec, path)
    else:
        print("Not implemented: Visualization for {}D data".format(len(features)))


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage:")
        print(
            "{} <Directory containing histograms> <BucketDef.json>".format(sys.argv[0]))
        sys.exit(1)

    # Read the bucket specifications
    bucket_specs = readBucketSpecs(sys.argv[2])
    for f in find_files_recursive(sys.argv[1], pattern="*.csv"):
        print(f)
        draw_histogram(f, bucket_specs)
