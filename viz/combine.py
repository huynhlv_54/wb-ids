#!/usr/bin/env python3

import pandas as pd

#total = 500000      # attack set
total = 244572463 # test set

dfs = list()
for i in range(1, 10):
    print(i)
    df = pd.read_csv("D{}-summary.csv".format(i))
    df.columns = ["D{}".format(i)]
    dfs.append(df)

    
df = pd.concat(dfs, axis=1)    
df = df / total
df.to_csv("DetectionRate.csv")
