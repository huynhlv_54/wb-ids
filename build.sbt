name := "wb-ids"
version := "0.1"

scalaVersion := "2.11.12"
scalaVersion in ThisBuild := "2.11.12"
ensimeScalaVersion in ThisBuild := "2.11.12"

val sparkVersion = "2.3.1"
val scalaTestVersion = "3.0.4"

resolvers += "Sonatype Releases" at "https://oss.sonatype.org/content/repositories/releases/"
resolvers += "Sonatype Snapshots" at "http://oss.sonatype.org/content/repositories/snapshots"

// Spark
libraryDependencies += "org.apache.spark" %% "spark-core"  % sparkVersion
libraryDependencies += "org.apache.spark" %% "spark-sql"   % sparkVersion
libraryDependencies += "org.apache.spark" %% "spark-mllib" % sparkVersion

// scalanlp
//libraryDependencies += "org.scalanlp" %% "breeze" % scalanlpVersion
//libraryDependencies += "org.scalanlp" %% "breeze-natives" % scalanlpVersion
// libraryDependencies += "org.scalanlp" %% "breeze-viz" % scalanlpVersion

// Json
libraryDependencies += "net.liftweb" %% "lift-json" %  "3.3.0-M1"

// Geocalc
libraryDependencies += "net.sf.geographiclib" % "GeographicLib-Java" % "1.49"

// another routing library: Graphhopper
libraryDependencies += "com.graphhopper" % "graphhopper-core" % "0.10.1"
libraryDependencies += "com.graphhopper" % "graphhopper-map-matching-core" % "0.10.0"
libraryDependencies += "com.graphhopper" % "graphhopper-reader-osm" % "0.10.1"


// For coordinate conversion
libraryDependencies += "org.osgeo" % "proj4j" % "0.1.0"

// Another library for coordinate conversion
resolvers ++= Seq(
  "maven2" at "http://download.java.net/maven/2",
  "osgeo" at "http://download.osgeo.org/webdav/geotools",
  "boundless" at "http://repo.boundlessgeo.com/main",
  "geosolutions" at "http://maven.geo-solutions.it/"
)

val gtVersion = "19.1"
libraryDependencies ++= Seq(
  "org.geotools" % "gt-metadata" % gtVersion,
  "org.geotools" % "gt-referencing" % gtVersion,
  "org.geotools" % "gt-epsg-wkt" % gtVersion,
  "org.geotools" % "gt-epsg-extension" % gtVersion,
  // This is one finicky dependency. Being explicit in hopes it will stop hurting Travis.
  "javax.media" % "jai_core" % "1.1.3" % Test from "http://download.osgeo.org/webdav/geotools/javax/media/jai_core/1.1.3/jai_core-1.1.3.jar"
)



// GeoSpark
// For finding vehicles in proximity
val geoSparkVer = "1.1.3"
libraryDependencies ++= Seq(
  "org.datasyslab" % "geospark" % geoSparkVer,
  "org.datasyslab" % "geospark-sql_2.3" % geoSparkVer
)

// Some global settings
cancelable in Global := true
logLevel in run := Level.Debug
fork := true

scalacOptions in ThisBuild ++= Seq("-unchecked", "-deprecation")

// put dependency jars to lib_managed
//retrieveManaged := true


enablePlugins(JavaAppPackaging)
