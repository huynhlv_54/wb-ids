# README #

Implementation of a white-box intrusion detection framework [1], adapted to Vehicle-to-Vehicle communication data. The program output (i.e., our analysis of the data) is documented in [2].

### Setup ###
To build this project, install [Scala SBT](https://www.scala-sbt.org)  and run

```sbt packageZipTarBall
```
	
at the project root directory. The compiled binaries will be stored in the targets/ directory.

Note: This repository does not contain the associated Vehicle-to-Vehicle communication data.


### References ###
[1] E. Costante, J. den Hartog, Milan Petković, S. Etalle, M. Pechenizkiy, "A white-box anomaly-based framework for database leakage detection",
Journal of Information Security and Applications, Volume 32, 2017, pp. 27-46.

[2] V. H. Le, J. den Hartog, N. Zannone, "Feature Selection for Anomaly Detection in Vehicular Ad Hoc Networks", in: Proceedings of the 15th
International Joint Conference on e-Business and Telecommunications, SciTePress, 2018, pp. 481–491